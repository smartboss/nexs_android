package com.nexstreaming.china.mediastudio.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.internal.ui.widget.BorderImageView;
import com.nexstreaming.china.mediastudio.internal.utils.Utils;
import com.nexstreaming.china.mediastudio.view.export.ExportActivity;
import com.nexstreaming.china.mediastudio.view.picker.PickerActivity;
import com.nexstreaming.china.mediastudio.view.export.CollageProgressDialog;
import com.nexstreaming.china.mediastudio.view.widget.PlayController;
import com.nexstreaming.nexeditorsdk.nexApplicationConfig;
import com.nexstreaming.nexeditorsdk.nexClip;
import com.nexstreaming.nexeditorsdk.nexCollageInfo;
import com.nexstreaming.nexeditorsdk.nexCollageInfoDraw;
import com.nexstreaming.nexeditorsdk.nexCollageInfoTitle;
import com.nexstreaming.nexeditorsdk.nexCollageManager;
import com.nexstreaming.nexeditorsdk.nexColorEffect;
import com.nexstreaming.nexeditorsdk.nexEngine;
import com.nexstreaming.nexeditorsdk.nexEngineListener;
import com.nexstreaming.nexeditorsdk.nexEngineView;
import com.nexstreaming.nexeditorsdk.nexExportFormat;
import com.nexstreaming.nexeditorsdk.nexExportFormatBuilder;
import com.nexstreaming.nexeditorsdk.nexExportListener;
import com.nexstreaming.nexeditorsdk.nexProject;
import com.nexstreaming.nexeditorsdkapis.ApiDemosConfig;
import com.nexstreaming.nexeditorsdkapis.common.ActivitySave;
import com.nexstreaming.nexeditorsdkapis.etc.CollageOverlayView;
import com.nexstreaming.nexfacedetection.nexFaceDetector;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.nexstreaming.china.mediastudio.view.CollageResultActivity.FILE_PATH;
import static com.nexstreaming.china.mediastudio.view.picker.PickerActivity.NAME_FUNCTION_TYPE;
import static com.nexstreaming.nexeditorsdk.nexApplicationConfig.letterbox_effect_black;

/**
 * Created by marklin on 2018/1/13.
 */

public class CollageActivity extends Activity implements View.OnClickListener, View.OnTouchListener, nexEngineView.NexViewListener, SeekBar.OnSeekBarChangeListener {
    public static final String TAG = CollageActivity.class.getSimpleName();
    public static final String RATIO = "ratio";
    public static final String COLLAGE_CHANGE_RESOURCE = "collage_change_resource";
    public static final String VIDEO_COUNT = "video_count";

    private final int REQUEST_CHANGE_RESOURCE = 100;

    private enum Status {
        STATUS_STYLE, STATUS_IMAGE_CONTROL
    }

    private static final int FLIP_ORIGINAL = 0;
    private static final int FLIP_HORIZONTAL = 1;
    public static final int FLIP_VERTICAL = 2;
    private int drawFlipStatus = FLIP_ORIGINAL;

    private Status mStatus = Status.STATUS_STYLE;

    private nexEngineView mView;
    private nexEngine mEngine;

    private FrameLayout mHolderOverlay;
    private CollageOverlayView mOverlayView;

    private nexCollageManager mKmCollageManager;
    private nexProject mProject;

    private nexCollageManager.Collage mCurCollage = null;
    private nexCollageManager.CollageType collageShowType = nexCollageManager.CollageType.ALL;
    private nexFaceDetector nexFD = null;

    private List<nexCollageManager.Collage> mCollages = new ArrayList<>();
    private ArrayList<String> mListFilePath;

    private nexCollageInfoDraw mCurCollageDrawInfo = null, mCurCollageDrawInfo2 = null;
    private nexCollageInfoTitle mCurCollageTitleInfo = null;

    private boolean mBackgroundMode = false;

    private int collageStoreSourceCount = 0;

    private ProgressDialog progressDialog;

    private CollageStyleAdapter mCollageStyleAdapter;
    private ColorEffectListAdapter mColorEffectListAdapter;

    private boolean isInitFinish = false;

    private int videoCount = 0;

    //generic UI
    private RecyclerView recyclerViewStyle, recyclerViewColorEffect;
    private LinearLayout llCollageControl, llStyleControl;
    private ImageView imgLoading;
    private ImageButton imgClose, imgBack, imgExport, btnRotate, btnMirrorHorizontal, btnMirrorVertical, btnChange;

    private TextView tvControlLeft, tvControlRight;
    private SeekBar seekBar;
    private boolean mUserControlling = false;

    private ImageView dragImage;
    private RectF ImageSize;
    private int DragImageHeight;
    private int DragImageWidth;


    // play controller
    private PlayController mPlayController = null;

    private void initPlayController() {
        mPlayController = new PlayController(mEngine, null, findViewById(R.id.play_controller_screen), (ImageView) findViewById(R.id.play_controller_status), (ImageButton) findViewById(R.id.play_controller_button));
        mPlayController.setListener(new PlayController.OnControlListener() {
            @Override
            public void onControlPause() {

            }

            @Override
            public void onControlPlay() {
                if (isInitFinish) {
                    mEngine.pause();
                    mEngine.seek(0);
                    mEngine.resume();
//                    mPlayController.setPlaying();

                    isInitFinish = false;
                    mPlayController.showScreen(true);
                }
            }
        });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApiDemosConfig.getApplicationInstance().initApp();
        setContentView(R.layout.activity_collage);
        initView();

        Intent intent = getIntent();
        boolean existSaveData = intent.getBooleanExtra("existsave", false);
        //record video and photo item count to check size when user change resource, and that PickerActivity can check item
        videoCount = intent.getIntExtra(VIDEO_COUNT, 0);

        if (existSaveData) {
            mProject = nexProject.createFromSaveString(ActivitySave.getPram2());
        } else {
            mListFilePath = intent.getStringArrayListExtra("filelist");
            mProject = new nexProject();

            for (int i = 0; i < mListFilePath.size(); i++) {
                nexClip clip = nexClip.getSupportedClip(mListFilePath.get(i));
                if (clip != null) {
                    mProject.add(clip);
                    int rotate = clip.getRotateInMeta();

                    if (clip.getClipType() == nexClip.kCLIP_TYPE_VIDEO) {
                        try {
                            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                            retriever.setDataSource(mListFilePath.get(i));
                            String rotation = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);

                            if (rotation != null) {
                                rotate = Integer.parseInt(rotation);
                            }
                            clip.setRotateDegree(360 - rotate);

                        } catch (RuntimeException e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                } else {
                    String skip = "Add content to project failed = " + mListFilePath.get(i);

                    Toast.makeText(getApplicationContext(), skip, Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Add content to project failed = " + mListFilePath.get(i));
                }
            }
        }

        collageStoreSourceCount = mProject.getTotalClipCount(true);
        refreshAssets();

        mEngine = ApiDemosConfig.getApplicationInstance().getEngine();
        mEngine.setView(mView);
        mEngine.set360VideoForceNormalView();

        initPlayController();

        if (mCollages.size() > 0) {
            imgLoading.setVisibility(View.VISIBLE);
            mCurCollage = mCollages.get(0);

            String errorMsg = setEffects2Project();
            if (errorMsg != null) {
                Toast.makeText(CollageActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                return;
            }

            showEngineView(false);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(CollageActivity.this);
            String message = "Not available collage with current source!!";
            builder.setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            builder.create().show();
        }
        mView.setListener(this);
        mOverlayView.setOnTouchListener(this);
    }

    @Override
    public void onEngineViewAvailable(int i, int i1) {
        if (!mBackgroundMode && mCurCollage != null) {
            String errorMsg = setEffects2Project();
            if (errorMsg != null) {
                return;
            }
            mEngine.faceDetect(true, mProject.getTotalClipCount(true), nexEngine.nexUndetectedFaceCrop.ZOOM);
            mEngine.play();
            mPlayController.setPlaying();
        }
    }

    @Override
    public void onEngineViewSizeChanged(int i, int i1) {

    }

    @Override
    public void onEngineViewDestroyed() {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        View view = getCurrentFocus();

        if (view != null) {
            //hide keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        float width = mOverlayView.getMeasuredWidth();
        float height = mOverlayView.getMeasuredHeight();


        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                nexCollageInfo info = null;
                if (mCurCollage != null) {
                    //get select collage image
                    info = mCurCollage.getCollageInfos(event.getX() / width, event.getY() / height);
                    updateControlUI(info, false);
                    if (mCurCollageDrawInfo != null) {
                        createDragPicture(info, event);
                        //拖曳時原始位置圖片隱藏
                        //mCurCollageDrawInfo.setVisible(!mCurCollageDrawInfo.getVisible());
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (mCurCollage != null && mCurCollageDrawInfo != null) {
                    dragImage.setX(event.getX() - DragImageWidth);
                    dragImage.setY(event.getY() - DragImageHeight);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mCurCollage != null) {
                    mHolderOverlay.removeView(dragImage);
                    info = mCurCollage.getCollageInfos(event.getX() / width, event.getY() / height);
                    if (mCurCollageDrawInfo != null) {
                        //放開時回復隱藏圖片
                        if (!mCurCollageDrawInfo.equals(info)) {
                            //isDrag用於是否以拖拉方式換圖
                            updateControlUI(info, true);
                        }
                    }

                }
                break;
        }
        return true;
    }

    private void createDragPicture(nexCollageInfo info, MotionEvent event) {
        dragImage = new ImageView(this);

        if (mCurCollageDrawInfo.getBindSource().getClipType() == nexClip.kCLIP_TYPE_VIDEO) {
            final nexClip tempNexClip = mCurCollageDrawInfo.getBindSource();
            getVideoBitmap(tempNexClip, new OnVideoBitmapCallback() {
                @Override
                public void getVideoBitmap(Bitmap bitmap) {
                    dragImage.setImageBitmap(bitmap);
                }
            });
        } else {
            Bitmap Thumbnail = mCurCollageDrawInfo.getBindSource().getMainThumbnail(240, getBaseContext().getResources().getDisplayMetrics().density);
            dragImage.setImageBitmap(Thumbnail);
        }

        dragImage.setAlpha(0.9f);
        //ScaleType 可以設置 CENTER 換為原始圖未切割前
        dragImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //獲取藍色方框尺寸(有些回傳不是那麼正確)
        ImageSize = info.getRectangle();
        DragImageHeight = (int) (Math.round(ImageSize.height() * 667));
        DragImageWidth = (int) (Math.round(ImageSize.width() * 667));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(DragImageWidth, DragImageHeight);
        dragImage.setLayoutParams(params);
        //取中心
        dragImage.setX(event.getX() - DragImageWidth);
        dragImage.setY(event.getY() - DragImageHeight);
        mHolderOverlay.addView(dragImage);
    }

    private void updateControlUI(final nexCollageInfo collageInfo, boolean isDrag) {
        if (mOverlayView.hasCollageInfo() == false) {
            return;
        }
        if (collageInfo == null) {
            //no focus image or title
            setCollageBottomView(Status.STATUS_STYLE);
            mCurCollageDrawInfo = null;
            mCurCollageTitleInfo = null;
        } else {
            if (collageInfo instanceof nexCollageInfoDraw) {
                //select item is collage image
                if (mCurCollageDrawInfo != null && mCurCollageDrawInfo.equals(collageInfo)) {
                    //cancel original select image
                    setCollageBottomView(Status.STATUS_STYLE);
                    mCurCollageDrawInfo = null;
                    mCurCollageTitleInfo = null;
                } else if (mCurCollageDrawInfo != null && isDrag) {
                    mCurCollage.swapDrawInfoClip(mCurCollageDrawInfo, (nexCollageInfoDraw) collageInfo);
                    setCollageBottomView(Status.STATUS_STYLE);

                    mCurCollageDrawInfo = null;
                    mCurCollageTitleInfo = null;
                    refreshEngine();
                } else {
                    setCollageBottomView(Status.STATUS_IMAGE_CONTROL);
                    mCurCollageDrawInfo = (nexCollageInfoDraw) collageInfo;
                    mCurCollageTitleInfo = null;
                    if (mCurCollageDrawInfo.getBindSource().getClipType() == nexClip.kCLIP_TYPE_VIDEO) {
                        final nexClip tempNexClip = mCurCollageDrawInfo.getBindSource();
                        getVideoBitmap(tempNexClip, new OnVideoBitmapCallback() {
                            @Override
                            public void getVideoBitmap(Bitmap bitmap) {
                                mColorEffectListAdapter.setSource(bitmap);
                            }
                        });
                    } else {
                        mColorEffectListAdapter.setSource(mCurCollageDrawInfo.getBindSource().getMainThumbnail(240, getBaseContext().getResources().getDisplayMetrics().density));
                    }
                }
            } else if (collageInfo instanceof nexCollageInfoTitle) {
                //select item is text title
                if (mCurCollageTitleInfo != null && mCurCollageTitleInfo.equals(collageInfo)) {
                    //cancel original select title
                    mCurCollageTitleInfo = null;
                    mCurCollageDrawInfo = null;
                } else {
                    mCurCollageTitleInfo = (nexCollageInfoTitle) collageInfo;
                    mCurCollageDrawInfo = null;
                    setCollageBottomView(Status.STATUS_STYLE);

                    CollageInputDialogFragment.newInstance(new Bundle()).showDialog(getFragmentManager(), mCurCollageTitleInfo, new CollageInputDialogFragment.OnDialogCallback() {
                        @Override
                        public void Confirm(String text) {
                            mCurCollageTitleInfo.setTitle(text);
                            updateControlUI(collageInfo, false);
                        }

                        @Override
                        public void Cancel() {
                            updateControlUI(collageInfo, false);
                        }
                    });
                }
            }
        }

        mOverlayView.setSelCollageInfo((nexCollageInfo) mCurCollageDrawInfo, (nexCollageInfo) mCurCollageTitleInfo);
        mOverlayView.invalidate();
    }

    private void refreshEngine() {
        if (!mBackgroundMode && mCurCollage != null) {
            String errorMsg = setEffects2Project();
            if (errorMsg != null) {
                return;
            }
            mEngine.faceDetect(true, mProject.getTotalClipCount(true), nexEngine.nexUndetectedFaceCrop.ZOOM);
            mEngine.seek(mCurCollage.getEditTime());
        }
    }

    private void getVideoBitmap(final nexClip clip, final OnVideoBitmapCallback onVideoBitmapCallback) {
        int rotate = 0;

        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(clip.getPath());
            String rotation = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);

            if (rotation != null) {
                rotate = Integer.parseInt(rotation);
            }
        } catch (RuntimeException e) {
            Log.e(TAG, e.toString());
        }

        final int finalRotate = rotate;
        clip.loadVideoClipThumbnails(new nexClip.OnLoadVideoClipThumbnailListener() {
            @Override
            public void onLoadThumbnailResult(int event) {
                if (event == nexClip.OnLoadVideoClipThumbnailListener.kEvent_Ok || event == nexClip.OnLoadVideoClipThumbnailListener.kEvent_loadCompleted) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(finalRotate);

                    Bitmap originalBitmap = clip.getMainThumbnail(240f, getBaseContext().getResources().getDisplayMetrics().density);
                    Bitmap finalBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight(), matrix, true);
                    onVideoBitmapCallback.getVideoBitmap(finalBitmap);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.export:
                CollageExportSelectDialogFragment.newInstance().showDialog(getFragmentManager(), new CollageExportSelectDialogFragment.OnExportDialogCallback() {
                    @Override
                    public void OnImageSelect() {
                        if (mCurCollage == null || !isInitFinish) {
                            Toast.makeText(CollageActivity.this, "Waiting...", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mEngine.pause();
                        mEngine.seek(mCurCollage.getEditTime());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mPlayController.setPaused();
                                exportJPG();
                            }
                        }, 500);

                    }

                    @Override
                    public void OnVideoSelect() {
                        if (mCurCollage == null) return;

                        Intent intent = new Intent();
                        intent.setClass(CollageActivity.this, ExportActivity.class);
                        intent.putExtra(CollageActivity.TAG, true);
                        intent.putExtra(RATIO, mCurCollage.getRatio());
                        startActivity(intent);
                    }
                });
                break;
            case R.id.img_close:
                if (mStatus.equals(Status.STATUS_IMAGE_CONTROL)) {
                    onBackPressed();
                }
                break;
            case R.id.btn_rotate:
                if (mCurCollageDrawInfo != null) {
                    int angle = mCurCollageDrawInfo.getRotate();
                    if (angle >= 360) {
                        angle -= 360;
                    }
                    if (angle <= -360) {
                        angle += 360;
                    }

                    mCurCollageDrawInfo.setRotate(nexCollageInfoDraw.kRotate_start, angle);

                    for (int i = 0; i < 90; i++) {
                        angle = angle + 1;
                        mCurCollageDrawInfo.setRotate(nexCollageInfoDraw.kRotate_doing, (int) angle);
                    }

                    mCurCollageDrawInfo.setRotate(nexCollageInfoDraw.kRotate_end, angle);
                }
                break;
            case R.id.btn_mirror_horizontal:
                if (mCurCollageDrawInfo.getRotate() == 90 || mCurCollageDrawInfo.getRotate() == 270) {
                    mCurCollageDrawInfo.setFlip(FLIP_VERTICAL);
                } else {
                    mCurCollageDrawInfo.setFlip(FLIP_HORIZONTAL);
                }
                break;
            case R.id.btn_mirror_vertical:
                if (mCurCollageDrawInfo.getRotate() == 90 || mCurCollageDrawInfo.getRotate() == 270) {
                    mCurCollageDrawInfo.setFlip(FLIP_HORIZONTAL);
                } else {
                    mCurCollageDrawInfo.setFlip(FLIP_VERTICAL);
                }
                break;
            case R.id.btn_change:
                btnChange.setTag(mCurCollageDrawInfo);

                if (mCurCollageDrawInfo.getBindSource().getClipType() == nexClip.kCLIP_TYPE_VIDEO) {
                    videoCount = videoCount - 1;
                }

                Intent intent = new Intent(CollageActivity.this, PickerActivity.class);
                intent.putExtra(NAME_FUNCTION_TYPE, R.id.collage);
                intent.putExtra(COLLAGE_CHANGE_RESOURCE, true);
                intent.putExtra(VIDEO_COUNT, videoCount);

                startActivityForResult(intent, REQUEST_CHANGE_RESOURCE);
                break;

        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mUserControlling = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        mEngine.pause();
        int duration = mEngine.getDuration();
        mEngine.seek(duration * progress / 100);
        mEngine.resume();
        mPlayController.setPlaying();
        mUserControlling = false;

        if (isInitFinish) {
            isInitFinish = false;
            mPlayController.showScreen(true);
        }
    }

    private void exportJPG() {
        Float ratio = mCurCollage.getRatio();

        int width = 3840;
        int height = 3840;
        if (ratio >= 1)
            height = (int) (width / ratio);
        else
            width = (int) (height * ratio);

        final File f = getExportFile(width, height, "jpeg");

        nexExportFormat format = nexExportFormatBuilder.Builder()
                .setType("jpeg")
                .setWidth(width)
                .setHeight(height)
                .setPath(f.getAbsolutePath())
                .setQuality(90)
                .build();

        imgLoading.setVisibility(View.VISIBLE);
        mEngine.export(format, new nexExportListener() {
            @Override
            public void onExportFail(nexEngine.nexErrorCode err) {
                Log.d(TAG, "onExportFail: " + err.toString());
                imgLoading.setVisibility(View.GONE);
            }

            @Override
            public void onExportProgress(int percent) {
                //imgLoading.setVisibility(View.GONE);
            }

            @Override
            public void onExportDone(Bitmap bitmap) {
                Log.d(TAG, "onExportDone: " + f.getAbsolutePath());
                imgLoading.setVisibility(View.GONE);
                CollageResultActivity.goResult(CollageActivity.this, f.getAbsolutePath(), true);
            }
        });
    }

    public View.OnClickListener onCollageStyleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateControlUI(null, false);

            final nexCollageManager.Collage changeCollage = (nexCollageManager.Collage) view.getTag();
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.show();
            }

            mEngine.stop(new nexEngine.OnCompletionListener() {
                @Override
                public void onComplete(int resultCode) {
                    if (!changeCollage.equals(mCurCollage)) {
                        mCurCollage = changeCollage;
                        String errorMsg = setEffects2Project();
                        if (errorMsg != null) {
                            return;
                        }
                    }

                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    mCollageStyleAdapter.setFocus(changeCollage);
                    mCollageStyleAdapter.notifyDataSetChanged();
                    setNexFaceModule();
                    mEngine.faceDetect(true, mProject.getTotalClipCount(true), nexEngine.nexUndetectedFaceCrop.ZOOM);
                    if (mCurCollage.getType() == nexCollageManager.CollageType.StaticCollage) {
                        mEngine.seek(mCurCollage.getEditTime());
                    } else {
                        mOverlayView.setCollageInfo(null);
                        mOverlayView.invalidate();
                        mEngine.play();
                        mPlayController.setPlaying();
                    }
                }
            });
        }
    };

    private View.OnClickListener onColorEffectClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String colorEffect = (String) view.getTag();
            if (TextUtils.isEmpty(colorEffect)) {
                mCurCollageDrawInfo.setLut(null);
            } else {
                mCurCollageDrawInfo.setLut(colorEffect);
            }

            mColorEffectListAdapter.setFocus(colorEffect);
            mColorEffectListAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mEngine.setEventHandler(sEngineListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBackgroundMode = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBackgroundMode = true;

        mEngine.stop();
        mPlayController.setPaused();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEngine.stop();

        nexApplicationConfig.setDefaultLetterboxEffect(letterbox_effect_black);
        nexApplicationConfig.setAspectMode(nexApplicationConfig.getAspectMode());

        ApiDemosConfig.getApplicationInstance().releaseAPP();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHANGE_RESOURCE && resultCode == Activity.RESULT_OK) {
            String filepath = data.getStringExtra(CollageActivity.TAG);
            videoCount = data.getIntExtra(VIDEO_COUNT, 0);

            if (mListFilePath != null) {
                mListFilePath.clear();
            }

            if (filepath != null) {
                imgLoading.setVisibility(View.VISIBLE);

                final nexClip clip = nexClip.getSupportedClip(filepath);
                nexCollageInfoDraw drawInfo = (nexCollageInfoDraw) btnChange.getTag();
                if (clip != null && drawInfo != null) {
                    drawInfo.changeSource(clip);

                    if (clip.getClipType() == nexClip.kCLIP_TYPE_VIDEO) {
                        getVideoBitmap(clip, new OnVideoBitmapCallback() {
                            @Override
                            public void getVideoBitmap(Bitmap bitmap) {
                                mColorEffectListAdapter.setSource(bitmap);
                            }
                        });
                    } else {
                        Bitmap bitmap = clip.getMainThumbnail(240f, getBaseContext().getResources().getDisplayMetrics().density);
                        mColorEffectListAdapter.setSource(bitmap);
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mStatus.equals(Status.STATUS_IMAGE_CONTROL) || mCurCollageDrawInfo != null || mCurCollageTitleInfo != null) {
            setCollageBottomView(Status.STATUS_STYLE);
            mCurCollageDrawInfo = null;
            mCurCollageTitleInfo = null;

            mOverlayView.setSelCollageInfo((nexCollageInfo) mCurCollageDrawInfo, (nexCollageInfo) mCurCollageTitleInfo);
            mOverlayView.invalidate();
        } else {
            super.onBackPressed();
        }
    }

    private void initView() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mKmCollageManager = nexCollageManager.getCollageManager(getApplicationContext(), getApplicationContext());

        progressDialog = new ProgressDialog(CollageActivity.this);
        progressDialog.setMessage("Waiting...");
        progressDialog.setCancelable(false);

        imgLoading = (ImageView) findViewById(R.id.img_loading);
        imgLoading.setVisibility(View.GONE);

        imgBack = (ImageButton) findViewById(R.id.back);
        imgBack.setOnClickListener(this);

        imgExport = (ImageButton) findViewById(R.id.export);
        imgExport.setOnClickListener(this);

        imgClose = (ImageButton) findViewById(R.id.img_close);
        imgClose.setOnClickListener(this);

        btnRotate = (ImageButton) findViewById(R.id.btn_rotate);
        btnRotate.setOnClickListener(this);

        btnMirrorHorizontal = (ImageButton) findViewById(R.id.btn_mirror_horizontal);
        btnMirrorHorizontal.setOnClickListener(this);

        btnMirrorVertical = (ImageButton) findViewById(R.id.btn_mirror_vertical);
        btnMirrorVertical.setOnClickListener(this);

        btnChange = (ImageButton) findViewById(R.id.btn_change);
        btnChange.setOnClickListener(this);

        llCollageControl = (LinearLayout) findViewById(R.id.ll_collage_control);
        llStyleControl = (LinearLayout) findViewById(R.id.ll_style);

        tvControlLeft = (TextView) findViewById(R.id.text_control_left);
        tvControlRight = (TextView) findViewById(R.id.text_control_right);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        seekBar.setOnSeekBarChangeListener(this);

        recyclerViewStyle = (RecyclerView) findViewById(R.id.recycler_style);
        mCollageStyleAdapter = new CollageStyleAdapter();
        recyclerViewStyle.setAdapter(mCollageStyleAdapter);

        recyclerViewColorEffect = (RecyclerView) findViewById(R.id.recycler_color_effect);
        mColorEffectListAdapter = new ColorEffectListAdapter();
        recyclerViewColorEffect.setAdapter(mColorEffectListAdapter);

        mView = (nexEngineView) findViewById(R.id.engineview);
        mView.setBlackOut(true);

        //the view overlay collage to show focus at the item user selected
        mOverlayView = new CollageOverlayView(getApplicationContext());
        mHolderOverlay = (FrameLayout) findViewById(R.id.holder_overlay);
        mHolderOverlay.addView(mOverlayView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        setCollageBottomView(Status.STATUS_STYLE);
    }

    private void setCollageBottomView(Status status) {
        mStatus = status;
        if (status.equals(Status.STATUS_STYLE)) {
            llCollageControl.setVisibility(View.GONE);
            imgClose.setVisibility(View.GONE);
            llStyleControl.setVisibility(View.VISIBLE);
        } else if (status.equals(Status.STATUS_IMAGE_CONTROL)) {
            llCollageControl.setVisibility(View.VISIBLE);
            imgClose.setVisibility(View.VISIBLE);
            llStyleControl.setVisibility(View.GONE);
        }
    }

    private File getExportFile(int wid, int hei, String ext) {
        String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();

        File exportDir = new File(sdCardPath + File.separator + "KM" + File.separator + "Export");
        exportDir.mkdirs();
        Calendar calendar = Calendar.getInstance();
        java.util.Date date = calendar.getTime();
        String export_time = (new SimpleDateFormat("yyMMdd_HHmmss").format(date));
        File exportFile = new File(exportDir, "NexEditor_" + wid + "X" + hei + "_" + export_time + "." + ext);
        return exportFile;
    }

    private nexEngineListener sEngineListener = new nexEngineListener() {
        @Override
        public void onStateChange(int i, int i1) {
        }

        @Override
        public void onTimeChange(int i) {
            tvControlLeft.setText(Utils.getTimeText(i));
            if (!mUserControlling) {
                seekBar.setProgress((int) (i * 100f / mEngine.getDuration()));
            }
        }

        @Override
        public void onSetTimeDone(int i) {
            showEngineView(true);
        }

        @Override
        public void onSetTimeFail(int i) {

        }

        @Override
        public void onSetTimeIgnored() {

        }

        @Override
        public void onEncodingDone(boolean b, int i) {

        }

        @Override
        public void onPlayEnd() {
            if (mEngine != null && mCurCollage != null) {
                mEngine.seek(mCurCollage.getEditTime());
                isInitFinish = true;

                mPlayController.setPaused();
                mPlayController.showScreen(false);
            }
        }

        @Override
        public void onPlayFail(int i, int i1) {
        }

        @Override
        public void onPlayStart() {
            if (imgLoading.getVisibility() == View.VISIBLE) {
                imgLoading.setVisibility(View.GONE);
            }

            showEngineView(false);

            if (mEngine != null) {
                int duration = mEngine.getDuration();
                tvControlRight.setText(Utils.getTimeText(duration));
            }
        }

        @Override
        public void onClipInfoDone() {

        }

        @Override
        public void onSeekStateChanged(boolean b) {

        }

        @Override
        public void onEncodingProgress(int i) {

        }

        @Override
        public void onCheckDirectExport(int i) {

        }

        @Override
        public void onProgressThumbnailCaching(int i, int i1) {

        }

        @Override
        public void onFastPreviewStartDone(int i, int i1, int i2) {

        }

        @Override
        public void onFastPreviewStopDone(int i) {

        }

        @Override
        public void onFastPreviewTimeDone(int i) {

        }

        @Override
        public void onPreviewPeakMeter(int iCts, int iPeakMeterValue) {

        }
    };

    private void refreshAssets() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mCollages != null) {
                    mCollages.clear();
                }

                if (mKmCollageManager != null) {
                    mKmCollageManager.loadCollage();
                    mCurCollage = null;

                    mCollages = mKmCollageManager.getCollages(mProject.getTotalClipCount(true), collageShowType);
                    mCollageStyleAdapter.setCollages(mCollages);

                    nexColorEffect.updatePluginLut();
                }
            }
        });
    }

    private void showEngineView(boolean showLayout) {
        mView.setVisibility(View.VISIBLE);
        if (showLayout) {
            if (mCurCollage != null) {
                mOverlayView.setCollageInfo(mCurCollage.getCollageInfos());
            } else {
                mOverlayView.setCollageInfo(null);
            }
        } else {
            mOverlayView.setCollageInfo(null);
        }

        mOverlayView.invalidate();
    }

    private void setNexFaceModule() {
        mEngine.setFaceModule("6a460d22-cd87-11e7-abc4-cec278b6b50a");
    }

    private Object collageApplyLock = new Object();

    private String setEffects2Project() {
        synchronized (collageApplyLock) {
            if (mProject.getTotalTime() <= 0) {
                return "Project is empty";
            }

            if (mCollages == null || mCurCollage == null) {
                return "Collage did not selected";
            }

            if (mCurCollage.getRatioMode() == 0) {
                return "Collage with unkonwn screen ratio";
            }

            // mEngine.clearProject();
            setNexFaceModule();
            if (mCurCollage.applyCollage2Project(mProject, mEngine, mCurCollage.getDuration(), getApplicationContext()) == false) {
                return "Fail to apply Collage on project";
            }

            mView.setVisibility(View.GONE);
            nexApplicationConfig.setAspectMode(mCurCollage.getRatioMode());
        }
        return null;
    }

    public class CollageStyleAdapter extends RecyclerView.Adapter<CollageStyleHolder> {
        private List<nexCollageManager.Collage> collages = new ArrayList<>();

        private nexCollageManager.Collage focusCollage;

        @Override
        public CollageStyleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collage_style, parent, false);

            return new CollageStyleHolder(view);
        }

        @Override
        public void onBindViewHolder(CollageStyleHolder holder, int position) {
            holder.imgStyle.setImageBitmap(collages.get(position).icon());
            holder.tvName.setText(collages.get(position).name(null));

            if (collages.get(position).equals(focusCollage)) {
                holder.imgStyle.setBorder(true);
                holder.tvName.setTextColor(getResources().getColor(R.color.collage_orange));
            } else {
                holder.imgStyle.setBorder(false);
                holder.tvName.setTextColor(Color.BLACK);
            }

            holder.itemView.setTag(collages.get(position));
            holder.itemView.setOnClickListener(onCollageStyleClickListener);
        }

        @Override
        public int getItemCount() {
            return collages.size();
        }

        public void setCollages(List<nexCollageManager.Collage> collages) {
            this.collages = collages;
            setFocus(collages.get(0));

            notifyDataSetChanged();
        }

        public void setFocus(nexCollageManager.Collage focusCollage) {
            this.focusCollage = focusCollage;
        }
    }

    public class ColorEffectListAdapter extends RecyclerView.Adapter<CollageStyleHolder> {
        private List<String> colorEffectList = new ArrayList<String>();
        private String colorEffect = "";
        private String[] effectNames;
        private Bitmap mBitmap;

        public ColorEffectListAdapter() {
            colorEffectList.add("");
            for (String s : nexColorEffect.getLutIds()) {
                colorEffectList.add(s);
            }
            effectNames = getResources().getStringArray(R.array.effect_names);
        }

        @Override
        public CollageStyleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_collage_style, parent, false);

            return new CollageStyleHolder(view);
        }

        @Override
        public void onBindViewHolder(CollageStyleHolder holder, int position) {
            if (mBitmap == null) {
                return;
            }

            String lutId = colorEffectList.get(position);
            nexColorEffect effect = nexColorEffect.getLutColorEffect(lutId);

            Bitmap bm = null;

            if (TextUtils.isEmpty(lutId)) {
                effect = nexColorEffect.getPresetList().get(0);
                holder.imgStyle.setColorFilter(new ColorMatrixColorFilter(effect.getColorMatrix()));
                bm = mBitmap;
            } else {
                bm = nexColorEffect.applyColorEffectOnBitmap(mBitmap, effect);
            }

            holder.imgStyle.setImageBitmap(bm);
            holder.tvName.setText(effectNames[position]);

            if (TextUtils.isEmpty(colorEffect) && position == 0) {
                holder.imgStyle.setBorder(true);
                holder.tvName.setTextColor(getResources().getColor(R.color.collage_orange));
            } else if (colorEffect.equals(colorEffectList.get(position))) {
                holder.imgStyle.setBorder(true);
                holder.tvName.setTextColor(getResources().getColor(R.color.collage_orange));
            } else {
                holder.imgStyle.setBorder(false);
                holder.tvName.setTextColor(Color.BLACK);
            }

            holder.itemView.setTag(lutId);
            holder.itemView.setOnClickListener(onColorEffectClickListener);
        }

        @Override
        public int getItemCount() {
            return colorEffectList.size();
        }

        public void setSource(Bitmap mBitmap) {
            this.mBitmap = mBitmap;

            setFocus(colorEffectList.get(0));
            notifyDataSetChanged();
        }

        public void setFocus(String focusEffect) {
            this.colorEffect = focusEffect;
        }
    }

    public class CollageStyleHolder extends RecyclerView.ViewHolder {
        public BorderImageView imgStyle;
        public TextView tvName;

        public CollageStyleHolder(View itemView) {
            super(itemView);

            imgStyle = (BorderImageView) itemView.findViewById(R.id.img_style);
            tvName = (TextView) itemView.findViewById(R.id.tv_style_name);
        }
    }

    private interface OnVideoBitmapCallback {
        void getVideoBitmap(Bitmap bitmap);
    }
}

