package com.nexstreaming.china.mediastudio.view.export;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nexstreaming.china.mediastudio.R;

import static android.widget.ListPopupWindow.MATCH_PARENT;

/**
 * Created by marklin on 2018/1/21.
 */

public class CollageProgressDialog extends ProgressDialog {
    public static final String TAG = CollageProgressDialog.class.getSimpleName();

    public CollageProgressDialog(Context context) {
        super(context);
    }

    public CollageProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    private ProgressBar progressBar;
    private TextView tvPercentage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_collage_export);
        getWindow().setLayout(MATCH_PARENT, MATCH_PARENT);

        progressBar = (ProgressBar) findViewById(R.id.progressbar_export);
        tvPercentage = (TextView) findViewById(R.id.percentage);

        setMax(100);
        setProgress(0);
        setCancelable(false);
    }


    @Override
    public void setProgress(int value) {
        super.setProgress(value);

        if (tvPercentage != null) {
            tvPercentage.setText("" + value + "%");
        }

        if (progressBar != null) {
            progressBar.setProgress(value);
        }
    }
}
