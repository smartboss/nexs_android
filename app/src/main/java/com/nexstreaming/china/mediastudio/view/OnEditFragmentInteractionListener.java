package com.nexstreaming.china.mediastudio.view;

import android.os.Bundle;

import com.nexstreaming.nexeditorsdk.nexClip;

/**
 * Created by dimple on 10/02/2018.
 */

public interface OnEditFragmentInteractionListener {
    void onFragmentInteraction(int action, Bundle data);
    void setBrightness(int value);
    void setSaturation(int value);
    void setContrast(int value);
    void setSpeed(int value);
    void getVideoBitmap(final VideoTrimActivity.OnVideoBitmapCallback onVideoBitmapCallback);
    void setColorEffect(String name);
}
