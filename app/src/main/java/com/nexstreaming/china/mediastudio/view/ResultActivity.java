package com.nexstreaming.china.mediastudio.view;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.internal.utils.Utils;
import com.nexstreaming.china.mediastudio.view.widget.PlayController;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener, PlayController.OnControlListener {

    private final static String TAG = ResultActivity.class.getSimpleName();

    public final static String NAME_FILEPATH = "name_filepath";

    public final static void goResult(Activity activity, String path) {
        Intent intent = new Intent(activity, ResultActivity.class);
        intent.putExtra(NAME_FILEPATH, path);
        activity.startActivity(intent);
    }



    private String mFilePath = null;

    private TextView mTextViewTimeLeft;
    private TextView mTextViewTimeRight;
    private SeekBar mSeekBarControl;
    private boolean mUserControlling = false;

//    private nexEngine mEngine = null;
//    private nexEngineView m_editorView;
//    private nexEngineListener mEditorListener = null;
//
//    private nexProject project = new nexProject();

    private VideoView mVideoView;
    private int mDuration = 0;
    private SeekBarUpdater mSeekBarUpdater;
    private boolean mLeft = false;

    // play controller
    private PlayController mPlayController = null;
    private void initPlayController() {
        mPlayController = new PlayController(null, mVideoView, findViewById(R.id.play_controller_screen), (ImageView) findViewById(R.id.play_controller_status), (ImageButton) findViewById(R.id.play_controller_button));
        mPlayController.setListener(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        mVideoView = (VideoView) findViewById(R.id.video_view);

        setupControl();

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.home).setOnClickListener(this);

        mFilePath = getIntent().getStringExtra(NAME_FILEPATH);
        if (TextUtils.isEmpty(mFilePath)) {
            Log.w(TAG, "mFilePath is null");
        } else {
            TextView tvPath = (TextView) findViewById(R.id.text_path);
            tvPath.setText(getString(R.string.result_filepath, mFilePath));
        }

        Uri videoUri = Uri.parse(mFilePath);
        mVideoView.setVideoURI(videoUri);
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mp) {
                mp.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(MediaPlayer mediaPlayer) {
//                        Log.e(TAG, "onSeekComplete");
                    }
                });
                mDuration = mVideoView.getDuration();
                mTextViewTimeRight.setText(Utils.getTimeText(mDuration));
            }
        });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mPlayController.setPaused();
            }
        });
        mVideoView.start();
        mSeekBarUpdater = new SeekBarUpdater();
        mSeekBarUpdater.execute();

        initPlayController();
        mPlayController.setPlaying();

//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                new MyAsync().execute();
//            }
//        }, 1000);
//        new MyAsync().execute();



//        nexClip clip=nexClip.getSupportedClip(mFilePath);
//        if( clip != null ){
//
//            clip.setRotateDegree(clip.getRotateInMeta());
//            project.add(clip);
//        }
//
//        for( int i = 0 ; i < project.getTotalClipCount(true) ; i++ ){
//            project.getClip(i, true).getCrop().randomizeStartEndPosition(false, nexCrop.CropMode.PAN_RAND);
//        }
//
//        mEditorListener = new nexEngineListener(){
//            @Override
//            public void onStateChange(int oldState, int newState) {
//
//            }
//
//            @Override
//            public void onTimeChange(int currentTime) {
//                mTextViewTimeLeft.setText(Utils.getTimeText(currentTime));
//                if (!mUserControlling) {
//                    mSeekBarControl.setProgress((int) (currentTime * 100f / mEngine.getDuration()));
//                }
//            }
//
//            @Override
//            public void onSetTimeDone(int currentTime) {
//
//            }
//
//            @Override
//            public void onSetTimeFail(int err) {
//
//            }
//
//            @Override
//            public void onSetTimeIgnored() {
//
//            }
//
//            @Override
//            public void onEncodingDone(boolean iserror, int result) {
//
//            }
//
//            @Override
//            public void onPlayEnd() {
//
//            }
//
//            @Override
//            public void onPlayFail(int err, int iClipID) {
//
//            }
//
//            @Override
//            public void onPlayStart() {
//                int duration = mEngine.getDuration();
////                Log.e(TAG, "duration: " + duration + ", text: " + Utils.getTimeText(duration));
//                mTextViewTimeRight.setText(Utils.getTimeText(duration));
//            }
//
//            @Override
//            public void onClipInfoDone() {
//
//            }
//
//            @Override
//            public void onSeekStateChanged(boolean isSeeking) {
//
//            }
//
//            @Override
//            public void onEncodingProgress(int percent) {
//
//            }
//
//            @Override
//            public void onCheckDirectExport(int result) {
//            }
//
//            @Override
//            public void onProgressThumbnailCaching(int progress, int maxValue) {
//
//            }
//
//            @Override
//            public void onFastPreviewStartDone(int err, int startTime, int endTime) {
//
//            }
//
//            @Override
//            public void onFastPreviewStopDone(int err) {
//
//            }
//
//            @Override
//            public void onFastPreviewTimeDone(int err) {
//
//            }
//
//            @Override
//            public void onPreviewPeakMeter(int iCts, int iPeakMeterValue) {
//
//            }
//        };
//
//        m_editorView = (nexEngineView)findViewById(R.id.engineview);
//        mEngine = ApiDemosConfig.getApplicationInstance().getEngine();
//        mEngine.setView(m_editorView);
//        mEngine.setProject(project);
//        mEngine.setEventHandler(mEditorListener);
//
//        mEngine.play();
    }

//    @Override
//    protected void onStart() {
//        Log.d(TAG, "onStart");
//
//        if(mEngine != null) {
//            mEngine.setEventHandler(mEditorListener);
//        }
//        super.onStart();
//    }
//
//    @Override
//    protected void onDestroy() {
//        Log.d(TAG, "onDestroy");
//        ApiDemosConfig.getApplicationInstance().releaseEngine();
//        super.onDestroy();
//    }


    @Override
    protected void onResume() {
        super.onResume();

        mLeft = false;
    }

    @Override
    protected void onPause() {
        super.onPause();

        mLeft = true;
        mVideoView.stopPlayback();
        mPlayController.setPaused();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.home:
                HomeActivity.goBackHome(this);
                break;
            default:
                break;
        }
    }

    private void setupControl() {
        mTextViewTimeLeft = findViewById(R.id.text_control_left);
        mTextViewTimeRight = findViewById(R.id.text_control_right);
        mSeekBarControl = findViewById(R.id.seekbar_control);
        mSeekBarControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                if (fromUser) {
//                    Log.e(TAG, "control progress: " + progress + ", fromUser: " + fromUser);
//                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
//                Log.e(TAG, "onStartTrackingTouch");
                mUserControlling = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
//                Log.e(TAG, "onStopTrackingTouch, progress: " + progress);

                mUserControlling = false;
                mVideoView.seekTo(mDuration * progress / 100);
                if (!mVideoView.isPlaying()) {
                    mVideoView.start();
                    mPlayController.setPlaying();
                    mSeekBarUpdater = new SeekBarUpdater();
                    mSeekBarUpdater.execute();
                }


            }
        });
    }

    @Override
    public void onControlPause() {
        mLeft = true;
    }

    @Override
    public void onControlPlay() {
        mLeft = false;
        mSeekBarUpdater = new SeekBarUpdater();
        mSeekBarUpdater.execute();
    }


    private class SeekBarUpdater extends AsyncTask<Void, Integer, Void> {
        int current = 0;
        @Override
        protected Void doInBackground(Void... params) {

            do {
                if (mLeft) {
                    break;
                }
                current = mVideoView.getCurrentPosition();
                try {
                    mTextViewTimeLeft.setText(Utils.getTimeText(current));

                    publishProgress((int) (current * 100 / mDuration));
//                    Log.e(TAG, "current: " + current + ", mDuration: " + mDuration);
//                    Log.e(TAG, "progress: " + (int) (current * 100 / mDuration) + ", mVideoView.isPlaying(): " + mVideoView.isPlaying());

                    if (mSeekBarControl.getProgress() >= 99) {
//                        Log.e(TAG, "EEEEEEEEEEEE");
                        mTextViewTimeLeft.setText(Utils.getTimeText(mDuration));
                        break;
                    }
                } catch (Exception e) {
                }
            } while (mSeekBarControl.getProgress() <= 100);

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

//            Log.e(TAG, "onProgressUpdate: " + values[0]);

            if (!mUserControlling) {
                mSeekBarControl.setProgress(values[0]);
            }
        }
    }
}
