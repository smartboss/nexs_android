package com.nexstreaming.china.mediastudio.view;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.view.widget.PlayController;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by marklin on 2018/1/20.
 */

public class CollageResultActivity extends Activity implements View.OnClickListener, PlayController.OnControlListener {
    public static final String TAG = CollageResultActivity.class.getSimpleName();
    public static final String FILE_PATH = "file_path";
    private String filePath = "";

    private LinearLayout llBgResource;
    private RelativeLayout rlVideoScreen;
    private ImageButton imgBack, imgFinish;
    private ImageView imgPhoto;
    private VideoView videoView;
    private TextView tvPath, tvFinish;
    private RecyclerView recyclerShare;

    private ShareRecyclerAdapter shareRecyclerAdapter;
    private boolean isVideo = false;
    private boolean isCollage = true;

    // play controller
    private PlayController mPlayController = null;

    //    private boolean mLeft = false;
    private void initPlayController() {
        mPlayController = new PlayController(null, videoView, findViewById(R.id.play_controller_screen), (ImageView) findViewById(R.id.play_controller_status), (ImageButton) findViewById(R.id.play_controller_button));
        mPlayController.setListener(this);
    }

    public static void goResult(Activity activity, String path, boolean isCollage) {
        Intent intent = new Intent();
        intent.setClass(activity, CollageResultActivity.class);
        intent.putExtra(CollageResultActivity.TAG, isCollage);
        intent.putExtra(FILE_PATH, path);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_result);
        filePath = getIntent().getExtras().getString(FILE_PATH, "");
        isCollage = getIntent().getBooleanExtra(CollageResultActivity.TAG, true);
        initView();
    }

    private void initView() {
        imgBack = (ImageButton) findViewById(R.id.back);
        imgBack.setOnClickListener(this);

        imgFinish = (ImageButton) findViewById(R.id.finish);
        imgFinish.setOnClickListener(this);

        imgPhoto = (ImageView) findViewById(R.id.img_photo);
        videoView = (VideoView) findViewById(R.id.video);

        tvPath = (TextView) findViewById(R.id.tv_file_path);
        tvPath.setText(String.format(getResources().getString(R.string.collage_result_filepath), filePath));

        tvFinish = (TextView) findViewById(R.id.tv_finish);
        llBgResource = (LinearLayout) findViewById(R.id.ll_resource_background);
        rlVideoScreen = (RelativeLayout) findViewById(R.id.rl_video_screen);

        if (isCollage) {
            //set CollageResult layout
            llBgResource.setBackgroundColor(getResources().getColor(R.color.collagePink));
            tvFinish.setTextColor(Color.WHITE);
            tvFinish.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.icon_confirm_white), null, null, null);
            imgFinish.setImageDrawable(getResources().getDrawable(R.drawable.icon_collage_home));

            LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(getResources().getDimensionPixelOffset(R.dimen.collage_result_width), getResources().getDimensionPixelOffset(R.dimen.collage_result_height));
            llParams.gravity = Gravity.CENTER_HORIZONTAL;
            llParams.topMargin = getResources().getDimensionPixelOffset(R.dimen.video_trim_result_margin_top);
            rlVideoScreen.setLayoutParams(llParams);

            RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            videoView.setLayoutParams(rlParams);
        } else {
            //set VideoTrimResult layout
            llBgResource.setBackgroundColor(getResources().getColor(R.color.background_dark));
            tvFinish.setTextColor(getResources().getColor(R.color.colorPink));
            tvFinish.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.icon_confirm_pink), null, null, null);
            imgFinish.setImageDrawable(getResources().getDrawable(R.drawable.icon_22));

            LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llParams.gravity = Gravity.CENTER_HORIZONTAL;
            llParams.topMargin = getResources().getDimensionPixelOffset(R.dimen.video_trim_result_margin_top);
            rlVideoScreen.setLayoutParams(llParams);

            RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            videoView.setLayoutParams(rlParams);
        }

        if (filePath.contains(".mp4")) {
            isVideo = true;
            videoView.setVisibility(View.VISIBLE);
            imgPhoto.setVisibility(View.GONE);
            videoView.setVideoPath(filePath);
            videoView.start();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
//                    mp.setLooping(true);

                    mPlayController.setPlaying();
                }
            });
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
//                    videoView.setVideoPath(filePath);
//                    videoView.start();
                    mPlayController.setPaused();
                }
            });
        } else {
            videoView.setVisibility(View.GONE);
            imgPhoto.setVisibility(View.VISIBLE);
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            imgPhoto.setImageBitmap(bitmap);
        }

        initPlayController();

        recyclerShare = (RecyclerView) findViewById(R.id.recycler_share);
        shareRecyclerAdapter = new ShareRecyclerAdapter();
        recyclerShare.setAdapter(shareRecyclerAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.finish:
                HomeActivity.goBackHome(this);
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView.isPlaying()) {
            videoView.pause();
            mPlayController.setPaused();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!videoView.isPlaying()) {
            videoView.resume();
            mPlayController.setPlaying();
        }
    }

    @Override
    public void onControlPause() {

    }

    @Override
    public void onControlPlay() {

    }

    public class ShareRecyclerAdapter extends RecyclerView.Adapter<ShareHolder> {
        private ArrayList<Map<String, Integer>> mapArrayList = new ArrayList<>();
        private final String IMAGE = "image";
        private final String CONTENT = "content";

        public ShareRecyclerAdapter() {
            Map<String, Integer> mapWeChat = new HashMap<>();
            Map<String, Integer> mapQQ = new HashMap<>();
            Map<String, Integer> mapSina = new HashMap<>();
            Map<String, Integer> mapFb = new HashMap<>();
            Map<String, Integer> more = new HashMap<>();

            mapWeChat.put(IMAGE, R.drawable.wechat);
            mapWeChat.put(CONTENT, R.string.wechat);
            mapArrayList.add(mapWeChat);

            mapQQ.put(IMAGE, R.drawable.icon_qq);
            mapQQ.put(CONTENT, R.string.share_qq);
            mapArrayList.add(mapQQ);
//
//            mapSina.put(IMAGE, R.drawable.sina);
//            mapSina.put(CONTENT, R.string.sina);
//            mapArrayList.add(mapSina);
//
//            mapFb.put(IMAGE, R.drawable.fb);
//            mapFb.put(CONTENT, R.string.fb);
//            mapArrayList.add(mapFb);

            more.put(IMAGE, R.drawable.more);
            more.put(CONTENT, R.string.more);
            mapArrayList.add(more);
        }

        @Override
        public ShareHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_share, parent, false);
            return new ShareHolder(view);
        }

        @Override
        public void onBindViewHolder(ShareHolder holder, int position) {
            int imageResource = mapArrayList.get(position).get(IMAGE);
            int contentResource = mapArrayList.get(position).get(CONTENT);

            holder.imgIcon.setOnClickListener(mOnClickListener);

            switch (position) {
                case 0:
                    if (isAppInstalled("com.tencent.mm")) {
                        holder.imgIcon.setImageResource(imageResource);
                    } else {
                        holder.imgIcon.setImageResource(R.drawable.wechat_gray);
                        holder.imgIcon.setOnClickListener(null);
                    }
                    break;
                case 1:
                    if (isAppInstalled("com.tencent.mobileqq")) {
                        holder.imgIcon.setImageResource(imageResource);
                    } else {
                        holder.imgIcon.setImageResource(R.drawable.icon_qq_gray);
                        holder.imgIcon.setOnClickListener(null);
                    }
                    break;
                case 2:
                    holder.imgIcon.setImageResource(imageResource);
                    break;
//                case 2:
//                    if (isAppInstalled("com.sina.weibo")) {
//                        holder.imgIcon.setImageResource(imageResource);
//                    } else {
//                        holder.imgIcon.setImageResource(R.drawable.sina_gray);
//                        holder.imgIcon.setOnClickListener(null);
//                    }
//                    break;
//                case 3:
//                    if (isAppInstalled("com.facebook.katana")) {
//                        holder.imgIcon.setImageResource(imageResource);
//                    } else {
//                        holder.imgIcon.setImageResource(R.drawable.fb_gray);
//                        holder.imgIcon.setOnClickListener(null);
//                    }
//                    break;
            }
            holder.tvContent.setText(getResources().getString(contentResource));
            holder.imgIcon.setTag(position);
        }

        @Override
        public int getItemCount() {
            return mapArrayList.size();
        }

        private boolean isAppInstalled(String uri) {
            PackageManager pm = getPackageManager();
            try {
                pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
            }

            return false;
        }
    }

    public class ShareHolder extends RecyclerView.ViewHolder {
        public ImageView imgIcon;
        public TextView tvContent;

        public ShareHolder(View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.img_icon);
            tvContent = (TextView) itemView.findViewById(R.id.tv_content);
        }
    }

    private android.view.View.OnClickListener mOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();
            ComponentName comp = null;
            switch (position) {
                case 0:
                    //ComponentName comp = new ComponentName("com.tencent.mm","com.tencent.mm.ui.tools.ShareToTimeLineUI");// only image
                    comp = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI");
                    break;
                case 1:
                    comp = new ComponentName("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity");
                    break;
                case 2:
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));//uri为你要分享的图片的uri
                    if (isVideo) {
                        intent.setType("video/*");
                    } else {
                        intent.setType("image/*");
                    }
                    startActivity(intent);
                    break;
            }
            if (comp != null) {
                initShareIntent(comp);
            }
        }
    };


    private void initShareIntent(ComponentName com) {
        Intent intent = new Intent();
        intent.setComponent(com);
        intent.setAction(Intent.ACTION_SEND);
        if (isVideo) {
            intent.setType("video/*");
        } else {
            intent.setType("image/*");
        }
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));//uri为你要分享的图片的uri
        startActivity(intent);
    }
}
