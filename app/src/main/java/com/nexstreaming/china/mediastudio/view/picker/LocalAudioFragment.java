package com.nexstreaming.china.mediastudio.view.picker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.nexeditorsdk.nexApplicationConfig;
import com.nexstreaming.nexeditorsdk.nexTemplateManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnAudioFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LocalAudioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocalAudioFragment extends Fragment {

    private final static String TAG = LocalAudioFragment.class.getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private List<String> mList = new ArrayList<String>();

    private RecyclerView mRecyclerView;
    private LocalAudioRecyclerViewAdapter mLocalAudioRecyclerViewAdapter;

    private OnAudioFragmentInteractionListener mListener;

    private Map<String, Integer> mMapTemplateAudioName = new HashMap<>();

    public LocalAudioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LocalAudioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LocalAudioFragment newInstance(String param1, String param2) {
        LocalAudioFragment fragment = new LocalAudioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        buildAudioNameRelationship();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_local_audio, container, false);

        mRecyclerView = rootView.findViewById(R.id.recycler_view);

        mList.clear();
        getBgm();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mLocalAudioRecyclerViewAdapter = new LocalAudioRecyclerViewAdapter(getContext());
        mRecyclerView.setAdapter(mLocalAudioRecyclerViewAdapter);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAudioFragmentInteractionListener) {
            mListener = (OnAudioFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    void refresh() {
        mLocalAudioRecyclerViewAdapter.notifyDataSetChanged();
    }

//    private boolean listAssetFiles(String path) {
//        String[] list;
//        try {
//            list = getContext().getAssets().list(path);
//            if (list.length > 0) {
//                // This is a folder
//                for (String file : list) {
//                    if (!listAssetFiles(path + "/" + file))
//                        return false;
//                    else {
//                        // This is a file
//                        if (file.endsWith(".mp3") || file.endsWith(".m4a")) {
//                            Log.e(TAG, "file: " + path + "/" + file);
//                            mListMp3.add(file);
//                        }
//                    }
//                }
//            }
//        } catch (IOException e) {
//            return false;
//        }
//
//        return true;
//    }

    private void buildAudioNameRelationship() {
        mMapTemplateAudioName.clear();

        // bgmId / id
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dbuilding2_live.audio.JSworks_swing_02", R.string.audio_local_name_jsworks_swing_02);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dbuilding_live.audio.JSworks_swing_01", R.string.audio_local_name_jsworks_swing_01);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dflag_live.audio.Tropical", R.string.audio_local_name_tropical);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dnews_live.audio.JSworks_swing_02", R.string.audio_local_name_jsworks_swing_02);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dpaper_live.audio.paper1", R.string.audio_local_name_paper1);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.3drollpaper_live.audio.dead", R.string.audio_local_name_dead);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.brix.nexeditor.template.90shomevideo.audio.RosyGirlMST2", R.string.audio_local_name_rosygirlmst2);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.epicopener.audio.epicopener", R.string.audio_local_name_epicopener);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.funkymove.audio.FunkyMove", R.string.audio_local_name_funkymove);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.maria.nexeditor.template.lifestyle.audio.Future_Bass_Lifestyle", R.string.audio_local_name_future_bass_lifestyle);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.midautumnfestival.audio.maf", R.string.audio_local_name_audio_maf);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.jiyeon.nexeditor.template.myownvlog.audio.myownvlog_bgm", R.string.audio_local_name_myownvlog_bgm);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.design.nexeditor.template.cinematic_c.audio.Specialdays_re1", R.string.audio_local_name_specialdays_re1);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.jiyeon.nexeditor.template.thewhiteview.audio.thewhiteview_bgm", R.string.audio_local_name_thewhiteview_bgm);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.brix.nexeditor.template.travel.audio.MySD_bgm", R.string.audio_local_name_mysd_bgm);
        mMapTemplateAudioName.put("com.nexstreaming.kmsdk.jieun.nexeditor.template.breeze.audio.0110_Breeze MST", R.string.audio_local_name_0110_breeze_mst);
    }

    private void getBgm() {
//        listAssetFiles("kmassets");

        List<nexTemplateManager.Template> templates = new ArrayList<>();
        nexTemplateManager kmTemplateManager = nexTemplateManager.getTemplateManager(getContext(), getContext());
        if (kmTemplateManager != null) {
            kmTemplateManager.loadTemplate();
            //mCurTemplate = null;
            for (nexTemplateManager.Template template : kmTemplateManager.getTemplates()) {
                if (template.aspect() == 16 / 9.0f && nexApplicationConfig.getAspectRatioMode() == nexApplicationConfig.kAspectRatio_Mode_16v9) {
                    templates.add(template);
                } else if (template.aspect() == 9 / 16.0f && nexApplicationConfig.getAspectRatioMode() == nexApplicationConfig.kAspectRatio_Mode_9v16) {
                    templates.add(template);
                } else if (template.aspect() == 1.0f && nexApplicationConfig.getAspectRatioMode() == nexApplicationConfig.kAspectRatio_Mode_1v1) {
                    templates.add(template);
                } else if (template.aspect() == 2.0f && nexApplicationConfig.getAspectRatioMode() == nexApplicationConfig.kAspectRatio_Mode_2v1) {
                    templates.add(template);
                } else if (template.aspect() == 1 / 2.0f && nexApplicationConfig.getAspectRatioMode() == nexApplicationConfig.kAspectRatio_Mode_1v2) {
                    templates.add(template);
                }

                Log.d(TAG, String.format("Template Activity load templates(%s) (%f)", template.name("en"), template.aspect()));
            }

            for (nexTemplateManager.Template template : templates) {
                String bgmId = template.defaultBGMId();
                if (!TextUtils.isEmpty(bgmId)) {
                    // filter out duplicated bgm
                    if ("com.nexstreaming.kmsdk.design.nexeditor.template.3dnews_live.audio.JSworks_swing_02".equals(bgmId)) {
                        continue;
                    }
                    mList.add(bgmId);
                }
            }
        }
    }

    private class LocalAudioRecyclerViewAdapter extends RecyclerView.Adapter<LocalAudioRecyclerViewAdapter.ViewHolder> {

        private LayoutInflater inflater;

        // data is passed into the constructor
        LocalAudioRecyclerViewAdapter(Context context) {
            this.inflater = LayoutInflater.from(context);
        }

        // inflates the cell layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.recycler_item_local_audio, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            String bgmId = getItem(position);
            if (bgmId == null) {
                holder.textView.setText(R.string.audio_picker_no_audio);
            } else {
                holder.textView.setText(getAudioName(bgmId));
            }

            final boolean selected = mListener == null ? false : mListener.isCurrentBgm(getItem(position));
            if (selected) {
                holder.textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
            } else {
                holder.textView.setTextColor(Color.WHITE);
            }
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return (mList == null ? 0 : mList.size() + 1);
        }

        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView textView;

            ViewHolder(View itemView) {
                super(itemView);

                textView = itemView.findViewById(R.id.title);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                onItemClicked(view, getAdapterPosition());
            }
        }

        // convenience method for getting data at click position
        private String getItem(int position) {
            if (position == 0) {
                return null;
            }
            return mList.get(position - 1);
        }

        private void onItemClicked(View view, int position) {
            if (mListener != null) {
                Bundle data = new Bundle();
                data.putString(AudioPickerActivity.KEY_PATH, getItem(position));
                mListener.onFragmentInteraction(AudioPickerActivity.ACTION_CHOOSE_LOCAL_BGM, data);
            }

            mLocalAudioRecyclerViewAdapter.notifyDataSetChanged();
        }

        private String getAudioName(String bgmId) {
            Object obj = mMapTemplateAudioName.get(bgmId);
            if (obj == null) {
                return bgmId;
            }

            int resId = (Integer) obj;
            return getString(resId);
        }

    }
}
