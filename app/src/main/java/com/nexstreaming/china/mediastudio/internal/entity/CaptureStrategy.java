package com.nexstreaming.china.mediastudio.internal.entity;

/**
 * Created by dimple on 22/12/2017.
 */

public class CaptureStrategy {
    public final boolean isPublic;
    public final String authority;

    public CaptureStrategy(boolean isPublic, String authority) {
        this.isPublic = isPublic;
        this.authority = authority;
    }
}
