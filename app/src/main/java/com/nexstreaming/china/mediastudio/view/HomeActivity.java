package com.nexstreaming.china.mediastudio.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.nexstreaming.china.mediastudio.BuildConfig;
import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.view.picker.PickerActivity;
import com.nexstreaming.nexeditorsdk.nexApplicationConfig;

import java.lang.reflect.Method;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = HomeActivity.class.getSimpleName();

    public final static void goBackHome(Activity activity) {
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    public static final int REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION = 0;
    public static final int REQ_PERMISSIONS_READ_EXTERNAL_STORAGE_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        findViewById(R.id.movie).setOnClickListener(this);
        findViewById(R.id.live_photo).setOnClickListener(this);
        findViewById(R.id.collage).setOnClickListener(this);
        findViewById(R.id.video).setOnClickListener(this);

        requestExternalStoragePermission();
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // version text
        TextView tvVersion = (TextView) findViewById(R.id.version_info);
        tvVersion.setText(getString(R.string.version_info_format, nexApplicationConfig.getSDKVersion(), BuildConfig.UI_VERSION));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

//        Log.e(TAG, "onRequestPermissionsResult, requestCode: " + requestCode + ", result:" + grantResults[0]);

        switch (requestCode) {
            case REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    showPhotoDialog(false);
//                    Log.e(TAG, "Request WRITE_EXTERNAL_STORAGE is granted.");
                    requestExternalStoragePermission();
                } else {
                    Log.e(TAG, "Request WRITE_EXTERNAL_STORAGE is denied.");
                }
            }
            break;

            case REQ_PERMISSIONS_READ_EXTERNAL_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    showPhotoDialog(false);
//                    Log.e(TAG, "Request READ_EXTERNAL_STORAGE is granted.");
                } else {
                    Log.e(TAG, "Request READ_EXTERNAL_STORAGE is denied.");
                }
                return;
            }

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.movie:
            case R.id.video:
            case R.id.collage:
            case R.id.live_photo:
                PickerActivity.goPicker(this, view.getId());
                break;
            default:
                break;
        }
    }

    private boolean requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
//                    Logger.d(activity, "Should alert user why WRITE_EXTERNAL_STORAGE is requested.");
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("aaa");
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.create().show();

                    return true;

                } else {
                    // No explanation needed, we can request the permission.
//                    Logger.d(activity, "Request WRITE_EXTERNAL_STORAGE.");
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION);

                    return true;
                }
            } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
//                    Logger.d(activity, "Should alert user why WRITE_EXTERNAL_STORAGE is requested.");
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("aaa");
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    REQ_PERMISSIONS_READ_EXTERNAL_STORAGE_PERMISSION);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.create().show();

                    return true;

                } else {
                    Log.e(TAG, "No explanation needed, we can request the permission.");
                    // No explanation needed, we can request the permission.
//                    Logger.d(activity, "Request WRITE_EXTERNAL_STORAGE.");
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION);

                    return true;
                }
            } else {
//                Log.e(TAG, "read permission already granted");
//                PickerActivity.goPicker(this);
//                finish();
                return false;
            }
        } else {
            return false;
        }
    }
}
