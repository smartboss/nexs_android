package com.nexstreaming.china.mediastudio.view;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.nexeditorsdk.nexOverlayManager;

import java.util.List;

import static com.nexstreaming.nexeditorsdkapis.common.Constants.SUB_TITLE;
import static com.nexstreaming.nexeditorsdkapis.common.Constants.TITLE;

/**
 * Created by marklin on 2018/3/7.
 */

public class TitleDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = TitleDialogFragment.class.getSimpleName();

    private static TitleDialogFragment titleDialogFragment;

    private List<nexOverlayManager.nexTitleInfo> mCurOverlayTitleInfos;
    private nexOverlayManager.nexTitleInfo titleInfo, subTitleInfo;

    private OnDialogCallback onDialogCallback;

    private EditText etTitle, etSubTitle;
    private ImageButton btnCancel, btnConfirm;

    public static TitleDialogFragment newInstance(String title, String subTitle) {
        if (titleDialogFragment == null) {
            titleDialogFragment = new TitleDialogFragment();
        }

        Bundle bundle = new Bundle();
        bundle.putString(TITLE, title);
        bundle.putString(SUB_TITLE, subTitle);

        titleDialogFragment.setArguments(bundle);
        return titleDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_collage_input, container, false);
        setCancelable(false);

        etTitle = (EditText) view.findViewById(R.id.et_message);
        etSubTitle = (EditText) view.findViewById(R.id.et_subtitle);
        etSubTitle.setVisibility(View.VISIBLE);

        btnCancel = (ImageButton) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

        btnConfirm = (ImageButton) view.findViewById(R.id.btn_confirm);
        btnConfirm.setImageDrawable(getResources().getDrawable(R.drawable.icon_ok_pink));
        btnConfirm.setOnClickListener(this);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();

        String title = getArguments().getString(TITLE, "");
        String subTitle = getArguments().getString(SUB_TITLE, "");
        etTitle.setText(TextUtils.isEmpty(title) ? TITLE : title);
        etSubTitle.setText(TextUtils.isEmpty(subTitle) ? SUB_TITLE : subTitle);

        if (mCurOverlayTitleInfos != null) {
            for (int i = 0; i != mCurOverlayTitleInfos.size(); ++i) {
                nexOverlayManager.nexTitleInfo info = mCurOverlayTitleInfos.get(i);
                if (info.getText().equals(TITLE)) {
                    etTitle.setFilters(getInputFilter(info));
                } else if (info.getText().equals(SUB_TITLE)) {
                    etSubTitle.setFilters(getInputFilter(info));
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
    }

    public void showDialog(FragmentManager fragmentManager, List<nexOverlayManager.nexTitleInfo> mCurOverlayTitleInfos, OnDialogCallback onDialogCallback) {
        show(fragmentManager, null);
        this.mCurOverlayTitleInfos = mCurOverlayTitleInfos;
        this.onDialogCallback = onDialogCallback;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                if (onDialogCallback != null) {
                    onDialogCallback.Cancel();
                }
                break;
            case R.id.btn_confirm:
                if (onDialogCallback != null) {
                    if (TextUtils.isEmpty(etTitle.getText().toString()) && TextUtils.isEmpty(etSubTitle.getText().toString())) {
                        onDialogCallback.Cancel();
                    } else {
                        onDialogCallback.Confirm(etTitle.getText().toString(), etSubTitle.getText().toString());
                    }
                }
                break;
        }

        dismiss();
    }

    private InputFilter[] getInputFilter(final nexOverlayManager.nexTitleInfo nexTitleInfo) {
        InputFilter[] inputFilters = new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                        int overlayWidth = nexTitleInfo.getOverlayWidth();
                        int currentWidth = nexTitleInfo.getTextWidth(dest.subSequence(0, dstart).toString() + source.toString());

                        if (overlayWidth > currentWidth) {
                            return null;
                        }

                        int keep = dend - dstart;

                        return source.subSequence(0, keep);
                    }
                }
        };

        return inputFilters;
    }

    public interface OnDialogCallback {
        void Confirm(String title, String subTitle);

        void Cancel();
    }
}
