package com.nexstreaming.china.mediastudio.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.internal.ui.widget.BorderImageView;
import com.nexstreaming.nexeditorsdk.nexColorEffect;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnEditFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterFragment extends Fragment implements VideoTrimActivity.OnVideoBitmapCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView mRecyclerView;
    private ColorEffectListAdapter mAdapter;

    private OnEditFragmentInteractionListener mListener;

    public FilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterFragment newInstance(String param1, String param2) {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filter, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_color_effect);
        mAdapter = new ColorEffectListAdapter();
        mRecyclerView.setAdapter(mAdapter);

        if (mListener != null) {
            mListener.getVideoBitmap(this);
        }

        return rootView;
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditFragmentInteractionListener) {
            mListener = (OnEditFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void getVideoBitmap(Bitmap bitmap) {
        mAdapter.setSource(bitmap);
    }


    public class ColorEffectListAdapter extends RecyclerView.Adapter<CollageStyleHolder> implements View.OnClickListener {
        private List<String> colorEffectList = new ArrayList<String>();
        private String colorEffect = "";
        private String[] effectNames;
        private Bitmap mBitmap = null;

        public ColorEffectListAdapter() {
            colorEffectList.add("");
            for (String s : nexColorEffect.getLutIds()) {
                colorEffectList.add(s);
            }
            effectNames = getResources().getStringArray(R.array.effect_names);
        }

        @Override
        public CollageStyleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_trim_filter, parent, false);

            return new CollageStyleHolder(view);
        }

        @Override
        public void onBindViewHolder(CollageStyleHolder holder, int position) {
            if (mBitmap == null) {
                return;
            }

            String lutId = colorEffectList.get(position);
            nexColorEffect effect = nexColorEffect.getLutColorEffect(lutId);

            Bitmap bm = null;

            if (TextUtils.isEmpty(lutId)) {
                effect = nexColorEffect.getPresetList().get(0);
                holder.imgStyle.setColorFilter(new ColorMatrixColorFilter(effect.getColorMatrix()));
                bm = mBitmap;
            } else {
                bm = nexColorEffect.applyColorEffectOnBitmap(mBitmap, effect);
            }

            holder.imgStyle.setImageBitmap(bm);
            holder.tvName.setText(effectNames[position]);

            if (TextUtils.isEmpty(colorEffect) && position == 0) {
                holder.imgStyle.setBorder(true);
                holder.tvName.setTextColor(getResources().getColor(R.color.collage_orange));
            } else if (colorEffect.equals(colorEffectList.get(position))) {
                holder.imgStyle.setBorder(true);
                holder.tvName.setTextColor(getResources().getColor(R.color.collage_orange));
            } else {
                holder.imgStyle.setBorder(false);
                holder.tvName.setTextColor(Color.WHITE);
            }

            holder.itemView.setTag(lutId);
            holder.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String colorEffect = (String) view.getTag();

            if (mListener != null) {
                mListener.setColorEffect(colorEffect);
            }

            setFocus(colorEffect);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            if (mBitmap == null) {
                return 0;
            }
            return colorEffectList.size();
        }

        public void setSource(Bitmap mBitmap) {
            this.mBitmap = mBitmap;

            setFocus(colorEffectList.get(0));
            notifyDataSetChanged();
        }

        public void setFocus(String focusEffect) {
            this.colorEffect = focusEffect;
        }
    }

    public class CollageStyleHolder extends RecyclerView.ViewHolder {
        public BorderImageView imgStyle;
        public TextView tvName;

        public CollageStyleHolder(View itemView) {
            super(itemView);

            imgStyle = (BorderImageView) itemView.findViewById(R.id.img_style);
            tvName = (TextView) itemView.findViewById(R.id.tv_style_name);
        }
    }
}
