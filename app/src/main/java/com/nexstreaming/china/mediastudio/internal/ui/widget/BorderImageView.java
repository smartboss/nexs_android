package com.nexstreaming.china.mediastudio.internal.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

import com.nexstreaming.china.mediastudio.R;

/**
 * Created by marklin on 2018/1/14.
 */

public class BorderImageView extends android.support.v7.widget.AppCompatImageView {
    private boolean addBorder = false;

    private Paint paint;

    public BorderImageView(Context context) {
        super(context);

        paint = new Paint();
        paint.setColor(getResources().getColor(R.color.collage_orange));
        paint.setStrokeWidth(3);
    }

    public BorderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint();
        paint.setColor(getResources().getColor(R.color.collage_orange));
        paint.setStrokeWidth(3);
    }

    public BorderImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        paint = new Paint();
        paint.setColor(getResources().getColor(R.color.collage_orange));
        paint.setStrokeWidth(3);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (addBorder) {
            Rect rec = canvas.getClipBounds();
            rec.bottom--;
            rec.right--;
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(rec, paint);
        }
    }

    public void setBorder(boolean addBorder) {
        this.addBorder = addBorder;
        invalidate();
    }
}
