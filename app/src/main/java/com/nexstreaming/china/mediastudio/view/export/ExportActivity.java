/******************************************************************************
 * File Name        : ExportActivity.java
 * Description      :
 *******************************************************************************
 * Copyright (c) 2002-2017 NexStreaming Corp. All rights reserved.
 * http://www.nexstreaming.com
 *
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
 * PURPOSE.
 ******************************************************************************/

package com.nexstreaming.china.mediastudio.view.export;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.internal.utils.Toast;
import com.nexstreaming.china.mediastudio.view.CollageActivity;
import com.nexstreaming.china.mediastudio.view.CollageResultActivity;
import com.nexstreaming.china.mediastudio.view.ResultActivity;
import com.nexstreaming.china.mediastudio.view.VideoTrimActivity;
import com.nexstreaming.nexeditorsdk.nexApplicationConfig;
import com.nexstreaming.nexeditorsdk.nexEngine;
import com.nexstreaming.nexeditorsdk.nexEngineListener;
import com.nexstreaming.nexeditorsdk.nexExportFormat;
import com.nexstreaming.nexeditorsdk.nexExportFormatBuilder;
import com.nexstreaming.nexeditorsdk.nexExportListener;
import com.nexstreaming.nexeditorsdkapis.ApiDemosConfig;
import com.nexstreaming.nexeditorsdkapis.common.Stopwatch;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class ExportActivity extends Activity {
    private static final String TAG = "ExportActivity";
    private nexEngine mEngine;
    private File mFile;

    private TextView mTextViewPercentage;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export);

        // set screen timeout to never
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mEngine = ApiDemosConfig.getApplicationInstance().getEngine();

        mTextViewPercentage = (TextView) findViewById(R.id.percentage);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar_export);
        mProgressBar.setProgress(0);
        mProgressBar.setMax(100);

        int width = 1280;
        int height = 720;

        final Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(CollageActivity.TAG) && bundle.getBoolean(CollageActivity.TAG, false)) {
            width = 1280;
            height = 1280;

            Float ratio = bundle.getFloat(CollageActivity.RATIO, 0.0F);
            if (ratio >= 1) {
                height = (int) (width / ratio);
            } else {
                width = (int) (height * ratio);
            }
        }

        mFile = getExportFile(width, height);

        final nexExportFormat format = nexExportFormatBuilder.Builder()
                .setType("mp4")
                .setVideoCodec(nexEngine.ExportCodec_AVC)
                .setVideoBitrate(6 * 1024 * 1024)
                .setVideoProfile(nexEngine.ExportProfile_AVCBaseline)
                .setVideoLevel(nexEngine.ExportAVCLevel31)
                .setVideoRotate(0)
                .setVideoFPS(30 * 100)
                .setWidth(width)
                .setHeight(height)
                .setAudioSampleRate(44100)
                .setMaxFileSize(Long.MAX_VALUE)
                .setPath(mFile.getAbsolutePath())
                .build();

        mEngine.export(format, new nexExportListener() {
            @Override
            public void onExportFail(nexEngine.nexErrorCode err) {
            }

            @Override
            public void onExportProgress(int percent) {
                if (percent <= 100) {
                    mTextViewPercentage.setText("" + percent + "%");
                    mProgressBar.setProgress(percent);
                }
            }

            @Override
            public void onExportDone(Bitmap bitmap) {
                if (bundle != null && bundle.containsKey(VideoTrimActivity.TAG) && bundle.getBoolean(VideoTrimActivity.TAG, false)) {
                    CollageResultActivity.goResult(ExportActivity.this, mFile.getAbsolutePath(), false);
                } else if (bundle != null && bundle.containsKey(CollageActivity.TAG) && bundle.getBoolean(CollageActivity.TAG, false)) {
                    CollageResultActivity.goResult(ExportActivity.this, mFile.getAbsolutePath(), true);
                } else {
                    ResultActivity.goResult(ExportActivity.this, mFile.getAbsolutePath());
                }
                finish();
            }
        });
    }

    private File getExportFile(int wid, int hei) {
        String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();


        File exportDir = new File(sdCardPath + File.separator + "KM" + File.separator + "Export");
        exportDir.mkdirs();
        Calendar calendar = Calendar.getInstance();
        java.util.Date date = calendar.getTime();
        String export_time = (new SimpleDateFormat("yyMMdd_HHmmss").format(date));
        File exportFile = new File(exportDir, "NexEditor_" + wid + "X" + hei + "_" + export_time + ".mp4");
        return exportFile;
    }

    @Override
    public void onBackPressed() {
    }
}
