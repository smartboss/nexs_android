package com.nexstreaming.china.mediastudio.view.picker;

import android.os.Bundle;

/**
 * Created by dimple on 09/01/2018.
 */

public interface OnAudioFragmentInteractionListener {
    void onFragmentInteraction(int action, Bundle data);
    String getCurrentBgmPath();
    boolean isCurrentBgm(final String path);
}
