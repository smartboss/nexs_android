package com.nexstreaming.china.mediastudio.internal.utils;

import android.content.Context;
import android.content.res.Resources;

/**
 * Created by dimple on 24/12/2017.
 */

public class Toast {

    public final static int LENGTH_LONG = android.widget.Toast.LENGTH_LONG;
    public final static int LENGTH_SHORT = android.widget.Toast.LENGTH_SHORT;

    private static boolean mEnabled = false;
    private static android.widget.Toast mToastCurrent = null;

    private boolean mShown = false;

    private Toast() {

    }

    public static Toast makeText(Context context, CharSequence text, int duration) {
        mToastCurrent = android.widget.Toast.makeText(context, text, duration);
        return new Toast();
    }

    public static Toast makeText(Context context, int resId, int duration) throws Resources.NotFoundException {
        mToastCurrent =  android.widget.Toast.makeText(context, resId, duration);
        return new Toast();
    }

    public void show() {
        if (mEnabled && mToastCurrent != null && mShown) {
            mToastCurrent.show();
            mToastCurrent = null;
            mShown = true;
        }
    }
}
