package com.nexstreaming.china.mediastudio.view.picker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.internal.entity.Album;
import com.nexstreaming.china.mediastudio.internal.entity.Item;
import com.nexstreaming.china.mediastudio.internal.ui.adapter.AlbumMediaAdapter;
import com.nexstreaming.china.mediastudio.internal.ui.adapter.AlbumsAdapter;
import com.nexstreaming.china.mediastudio.internal.ui.widget.MediaGridInset;
import com.nexstreaming.china.mediastudio.internal.utils.ConvertFilepathUtil;
import com.nexstreaming.china.mediastudio.internal.utils.Toast;
import com.nexstreaming.china.mediastudio.internal.utils.Utils;
import com.nexstreaming.china.mediastudio.model.AlbumCollection;
import com.nexstreaming.china.mediastudio.model.AlbumMediaCollection;
import com.nexstreaming.china.mediastudio.view.CollageActivity;
import com.nexstreaming.china.mediastudio.view.Template20Activity;
import com.nexstreaming.china.mediastudio.view.VideoTrimActivity;
import com.nexstreaming.nexeditorsdkapis.etc.Template20TestActivity;
import com.nexstreaming.nexeditorsdkapis.surface.EngineViewTestActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.nexstreaming.china.mediastudio.view.CollageActivity.COLLAGE_CHANGE_RESOURCE;
import static com.nexstreaming.china.mediastudio.view.CollageActivity.VIDEO_COUNT;

public class PickerActivity extends AppCompatActivity implements AlbumCollection.AlbumCallbacks, AlbumMediaCollection.AlbumMediaCallbacks, AlbumMediaAdapter.CheckStateListener,
        AlbumMediaAdapter.OnMediaClickListener, View.OnClickListener {

    private final static String TAG = PickerActivity.class.getSimpleName();
    private final static int REQ_TEMPLATE_20 = 10000;
    private final static int REQ_COLLAGE_ACTIVITY = 10001;

    public final static String NAME_FUNCTION_TYPE = "name_function_type";

    public final static void goPicker(Activity activity, int type) {
        Intent intent = new Intent(activity, PickerActivity.class);
        intent.putExtra(NAME_FUNCTION_TYPE, type);
        activity.startActivity(intent);
    }

    public final static void goBackPicker(Activity activity) {
        Intent intent = new Intent(activity, PickerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    private enum MODE {
        NORMAL,
        REPICK
    }


    private final AlbumCollection mAlbumCollection = new AlbumCollection();


    private MODE mMode = MODE.NORMAL;
    private int mType;

    private AlbumsAdapter mAlbumsAdapter;

    private final AlbumMediaCollection mAlbumMediaCollection = new AlbumMediaCollection();
    private RecyclerView mRecyclerView;
    private AlbumMediaAdapter mAdapter;
    private Album mAlbum = null;

    private HorizontalScrollView mHorizontalScrollView;
    private LinearLayout mSelectedContainer;
    private TextView mTextViewSelectedCount;
    private Set<Item> mSelectedItem = new LinkedHashSet<>();

    private ImageButton mImageButtonApply;
    private ImageView mImageViewLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);

        mImageViewLoading = (ImageView) findViewById(R.id.img_loading);

        mType = getIntent().getIntExtra(NAME_FUNCTION_TYPE, 0);
        findViewById(R.id.back).setOnClickListener(this);

        if (mType == R.id.live_photo || mType == R.id.video) {
            findViewById(R.id.footer).setVisibility(View.GONE);
        } else if (mType == R.id.collage && getIntent().getExtras().containsKey(COLLAGE_CHANGE_RESOURCE)) {
            //when collage change resource, footer should be hidden
            boolean changeResource = getIntent().getBooleanExtra(COLLAGE_CHANGE_RESOURCE, false);
            findViewById(R.id.footer).setVisibility(changeResource ? View.GONE : View.VISIBLE);
        }

        mImageButtonApply = findViewById(R.id.apply);
        if (mType == R.id.collage) {
            mImageButtonApply.setImageResource(R.drawable.image_picker_apply_collage);
        }
        mImageButtonApply.setOnClickListener(this);
        mImageButtonApply.setEnabled(false);

        mTextViewSelectedCount = (TextView) findViewById(R.id.selected_photo_desc);
        mHorizontalScrollView = (HorizontalScrollView) findViewById(R.id.h_scroll_view);
        mSelectedContainer = (LinearLayout) findViewById(R.id.selected_item_container);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new AlbumMediaAdapter(this, mSelectedItem/*mSelectionProvider.provideSelectedItemCollection()*/, mRecyclerView);
        mAdapter.registerCheckStateListener(this);
        mAdapter.registerOnMediaClickListener(this);
        mRecyclerView.setHasFixedSize(true);
        int spanCount = 3;
//        SelectionSpec selectionSpec = SelectionSpec.getInstance();
//        if (selectionSpec.gridExpectedSize > 0) {
//            spanCount = UIUtils.spanCount(getContext(), selectionSpec.gridExpectedSize);
//        } else {
//            spanCount = selectionSpec.spanCount;
//        }


        mRecyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));

        int spacing = getResources().getDimensionPixelSize(R.dimen.media_grid_spacing);
        mRecyclerView.addItemDecoration(new MediaGridInset(spanCount, spacing, false));
        mRecyclerView.setAdapter(mAdapter);

        if (mType == R.id.video) {
            mAlbumMediaCollection.onCreate(this, this, true);
        } else {
            mAlbumMediaCollection.onCreate(this, this);
        }
//        mAlbumMediaCollection.load(album, selectionSpec.capture);

        mAlbumsAdapter = new AlbumsAdapter(this, null, false);

        mAlbumCollection.onCreate(this, this);
        mAlbumCollection.onRestoreInstanceState(savedInstanceState);
        mAlbumCollection.loadAlbums();

        setFooterVisibility();

        TextView tvHint = findViewById(R.id.hint);
        switch (mType) {
            case R.id.collage:
                tvHint.setText(R.string.picker_hint_collage);
                break;
            case R.id.video:
            case R.id.movie:
                tvHint.setText(R.string.picker_hint_template20);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mImageViewLoading.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PickerActivity.REQ_TEMPLATE_20) {
            if (resultCode == RESULT_OK) {
                mMode = MODE.REPICK;
            } else {
                finish();
            }
        }
    }

    private void goTemplate20() {
        Intent intent = new Intent(this, Template20Activity.class);

        ArrayList<String> list = new ArrayList<>();
        for (Item it : getSelectedItemAsList()) {
//            Log.e(TAG, "item: " + it.uri.toString());
//            Log.e(TAG, "item: " + ConvertFilepathUtil.getPath(this, it.uri));
            list.add(ConvertFilepathUtil.getPath(this, it.uri));
        }

        if (list.size() == 0) {
            Log.e(TAG, "selected image count is 0");
        } else {
            intent.putStringArrayListExtra("filelist", list);
            intent.putExtra("existsave", false);
            intent.putExtra(NAME_FUNCTION_TYPE, mType);
            startActivityForResult(intent, REQ_TEMPLATE_20);
        }
    }

    private void goCollageActivity() {
        Intent intent = new Intent(this, CollageActivity.class);
        int videoCount = 0;

        ArrayList<String> list = new ArrayList<>();
        for (Item it : getSelectedItemAsList()) {
            list.add(ConvertFilepathUtil.getPath(this, it.uri));
            if (it.isVideo()) {
                videoCount = videoCount + 1;
            }
        }

        if (list.size() == 0) {
            Toast.makeText(PickerActivity.this, "selected image count is 0", Toast.LENGTH_SHORT).show();
        } else {
            intent.putStringArrayListExtra("filelist", list);
            intent.putExtra(VIDEO_COUNT, videoCount);

            startActivity(intent);
        }
    }

    private void goVideoTrimActivity() {
        //   Intent intent = new Intent(this, VideoTrimActivity.class);
        Intent intent = new Intent(this, EngineViewTestActivity.class);

        ArrayList<String> list = new ArrayList<>();
        for (Item it : getSelectedItemAsList()) {
//            Log.e(TAG, "item: " + it.uri.toString());
//            Log.e(TAG, "item: " + ConvertFilepathUtil.getPath(this, it.uri));
            list.add(ConvertFilepathUtil.getPath(this, it.uri));
        }

        if (list.size() == 0) {
            Log.e(TAG, "selected image count is 0");
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.putStringArrayListExtra("filelist", list);
            intent.putExtra("existsave", false);
            startActivity(intent);
            //startActivityForResult(intent, REQ_TEMPLATE_20);
            finish();
        }
    }

    private void backToCollageActivity(Item item) {
        int videoCount = getIntent().getIntExtra(VIDEO_COUNT, 0);

        if (item.isVideo()) {
            videoCount = videoCount + 1;
            if (videoCount > 2) {
                showDialog(getResources().getString(R.string.picker_title_hint), getResources().getString(R.string.picker_content_video), getResources().getString(R.string.ok), "");
                return;
            }
        }

        Intent intent = new Intent();
        intent.putExtra(CollageActivity.TAG, ConvertFilepathUtil.getPath(this, item.uri));
        intent.putExtra(VIDEO_COUNT, videoCount);

        setResult(RESULT_OK, intent);
        finish();
    }

    private void goTemplate20Test() {
        Intent intent = new Intent(this, Template20TestActivity.class);

        ArrayList<String> list = new ArrayList<>();
        for (Item it : getSelectedItemAsList()) {
//            Log.e(TAG, "item: " + it.uri.toString());
//            Log.e(TAG, "item: " + ConvertFilepathUtil.getPath(this, it.uri));
            list.add(ConvertFilepathUtil.getPath(this, it.uri));
        }

        intent.putStringArrayListExtra("filelist", list);
        intent.putExtra("existsave", false);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (mMode == MODE.NORMAL) {
            super.onBackPressed();
        } else if (mMode == MODE.REPICK) {
            mMode = MODE.NORMAL;
            next();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.apply:
//                for (Item it : getSelectedItemAsList()) {
//                    Log.e(TAG, "item: " + it.uri.toString());
//                }
//                ResultActivity.goResult(this, getSelectedItemAsList());


//                ArrayList<Uri> selectedUris = (ArrayList<Uri>) mSelectedCollection.asListOfUri();
//                result.putParcelableArrayListExtra(EXTRA_RESULT_SELECTION, selectedUris);
//                ArrayList<String> selectedPaths = (ArrayList<String>) mSelectedCollection.asListOfString();
//                result.putStringArrayListExtra(EXTRA_RESULT_SELECTION_PATH, selectedPaths);
//                finish();

                next();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        mSelectedCollection.onSaveInstanceState(outState);
        mAlbumCollection.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAlbumCollection.onDestroy();
    }

    @Override
    public void onAlbumLoad(final Cursor cursor) {
//        Log.e(TAG, "onAlbumLoad, cursor size: " + cursor.getCount());
        mAlbumsAdapter.swapCursor(cursor);
        // select default album.
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                cursor.moveToPosition(mAlbumCollection.getCurrentSelection());
//                mAlbumsSpinner.setSelection(MatisseActivity.this, mAlbumCollection.getCurrentSelection());
                Album album = Album.valueOf(cursor);
//                if (album.isAll() && SelectionSpec.getInstance().capture) {
//                    album.addCaptureCount();
//                }
                onAlbumSelected(album);

                ((TextView) findViewById(R.id.album_name)).setText(album.getDisplayName(PickerActivity.this));
            }
        });
    }

    @Override
    public void onAlbumReset() {
        mAlbumsAdapter.swapCursor(null);
    }

    @Override
    public void onAlbumMediaLoad(Cursor cursor) {
//        Log.e(TAG, "onAlbumMediaLoad, cursor size: " + cursor.getCount());
        mAdapter.swapCursor(cursor);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAlbumMediaReset() {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onUpdate() {
        // notify outer Activity that check state changed
//        if (mCheckStateListener != null) {
//            mCheckStateListener.onUpdate();
//        }
    }

    @Override
    public void onMediaClick(Album album, Item item, int adapterPosition) {
//        Log.e(TAG, "onMediaClick adapterPosition: " + adapterPosition);
//        if (mOnMediaClickListener != null) {
//            mOnMediaClickListener.onMediaClick((Album) getArguments().getParcelable(EXTRA_ALBUM), item, adapterPosition);
//        }
        if (mType == R.id.collage) {
            if (getIntent().getExtras().containsKey(COLLAGE_CHANGE_RESOURCE) && getIntent().getBooleanExtra(COLLAGE_CHANGE_RESOURCE, false)) {
                //change resource from collage
                backToCollageActivity(item);
            } else {
                //new action
                if (checkCollageList(item)) {
                    updateSelectedItem(item);
                }
            }
        } else if (mType == R.id.live_photo) {
            mSelectedItem.clear();
            updateSelectedItem(item);
            next();
        } else if (mType == R.id.video) {
            mSelectedItem.clear();
            updateSelectedItem(item);
            next();
        } else {
            updateSelectedItem(item);
        }
    }

    //collage can only select at most two video and 9 item
    private boolean checkCollageList(Item selectItem) {
        if (mSelectedItem.contains(selectItem)) {
            //remove item from list, always return true
            return true;
        }

        int videoSize = 0;
        if (selectItem.isVideo()) {
            for (Item it : getSelectedItemAsList()) {
                //check video size
                if (it.isVideo()) {
                    videoSize++;
                    if (videoSize >= 2) {
                        showDialog(getResources().getString(R.string.picker_title_hint), getResources().getString(R.string.picker_content_video), getResources().getString(R.string.ok), "");
                        return false;
                    }
                }
            }
        }

        if (getSelectedItemAsList().size() >= 9) {
            //check item size
            showDialog(getResources().getString(R.string.picker_title_hint), getResources().getString(R.string.picker_content_resource), getResources().getString(R.string.ok), "");
            return false;
        }

        return true;
    }

    private void showDialog(String title, String content, String positiveButton, String negativeButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(content);
        builder.setCancelable(false);
        if (!TextUtils.isEmpty(positiveButton)) {
            builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
        }

        if (!TextUtils.isEmpty(negativeButton)) {
            builder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
        }

        builder.create().show();
    }

    private void next() {
        mImageViewLoading.setVisibility(View.VISIBLE);
        switch (mType) {
            case R.id.live_photo:
                goTemplate20();
                break;
            case R.id.movie:
                goTemplate20();
                break;
            case R.id.collage:
                goCollageActivity();
                break;
            case R.id.video:
                goVideoTrimActivity();
                break;
            default:
                // do nothing
                break;
        }
    }

    private void onAlbumSelected(Album album) {
//        Log.e(TAG, "onAlbumSelected album: " + album.toString());
        mAlbum = album;
        if (album.isAll() && album.isEmpty()) {
//            Log.e(TAG, "all && empty");
//            mContainer.setVisibility(View.GONE);
//            mEmptyView.setVisibility(View.VISIBLE);
        } else {
//            Log.e(TAG, "others");
//            mContainer.setVisibility(View.VISIBLE);
//            mEmptyView.setVisibility(View.GONE);
//            Fragment fragment = MediaSelectionFragment.newInstance(album);
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.container, fragment, MediaSelectionFragment.class.getSimpleName())
//                    .commitAllowingStateLoss();


            mAlbumMediaCollection.load(mAlbum, /*selectionSpec.capture*/false, mType == R.id.live_photo);
        }
    }

    private void setFooterVisibility() {
//        mTextViewSelectedCount.setText(getString(R.string.count_selected_photo, mSelectedItem.size()));
//        findViewById(R.id.footer).setVisibility(mSelectedItem.size() == 0 ? View.INVISIBLE : View.VISIBLE);
    }

    private List<Item> getSelectedItemAsList() {
        return new ArrayList<>(mSelectedItem);
    }

    private void removeSelectedItem(final Item item) {
        for (int i = 0; i < getSelectedItemAsList().size(); ++i) {
            Item it = getSelectedItemAsList().get(i);
            if (it.equals(item)) {
                mSelectedContainer.removeViewAt(i);
                break;
            }
        }
        mSelectedItem.remove(item);
        setFooterVisibility();
        mAdapter.notifyDataSetChanged();
    }

    private void updateSelectedItem(final Item item) {
        if (item != null) {
            if (mSelectedItem.contains(item)) {
                removeSelectedItem(item);
            } else {
                final RelativeLayout rl = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.album_selected_item, null);
                final int width = Utils.dp2px(getResources(), 76.0f);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, width);
                rl.setLayoutParams(lp);

                ImageView iv = (ImageView) rl.findViewById(R.id.item_photo);
                Picasso.with(this)
                        .load(item.getContentUri())
                        .resize(width, width)
                        .centerCrop()
//                    .placeholder(R.drawable.user_placeholder)
//                    .error(R.drawable.user_placeholder_error)
                        .into(iv);
                rl.findViewById(R.id.item_delete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        removeSelectedItem(item);
                        mImageButtonApply.setEnabled(mSelectedItem.size() != 0);
                    }
                });
                rl.findViewById(R.id.item_photo).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        removeSelectedItem(item);
                        mImageButtonApply.setEnabled(mSelectedItem.size() != 0);
                    }
                });

                mSelectedItem.add(item);
                setFooterVisibility();
                mSelectedContainer.addView(rl);
                mAdapter.notifyDataSetChanged();
            }

            mHorizontalScrollView.post(new Runnable() {
                public void run() {
                    mHorizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            });

            mImageButtonApply.setEnabled(mSelectedItem.size() != 0);
        }
    }


//    private boolean requestExternalStoragePermission() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                // Should we show an explanation?
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//
//                    // Show an explanation to the user *asynchronously* -- don't block
//                    // this thread waiting for the user's response! After the user
//                    // sees the explanation, try again to request the permission.
////                    Logger.d(activity, "Should alert user why WRITE_EXTERNAL_STORAGE is requested.");
//                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
//                    builder.setMessage("aaa");
//                    builder.setCancelable(false);
//                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                    REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION);
//                        }
//                    });
//                    builder.setNegativeButton(R.string.cancel, null);
//                    builder.create().show();
//
//                    return true;
//
//                } else {
//                    // No explanation needed, we can request the permission.
////                    Logger.d(activity, "Request WRITE_EXTERNAL_STORAGE.");
//                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION);
//
//                    return true;
//                }
//            } else
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                // Should we show an explanation?
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                    // Show an explanation to the user *asynchronously* -- don't block
//                    // this thread waiting for the user's response! After the user
//                    // sees the explanation, try again to request the permission.
////                    Logger.d(activity, "Should alert user why WRITE_EXTERNAL_STORAGE is requested.");
//                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
//                    builder.setMessage("aaa");
//                    builder.setCancelable(false);
//                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                                    REQ_PERMISSIONS_READ_EXTERNAL_STORAGE_PERMISSION);
//                        }
//                    });
//                    builder.setNegativeButton(R.string.cancel, null);
//                    builder.create().show();
//
//                    return true;
//
//                } else {
//                    Log.e(TAG, "No explanation needed, we can request the permission.");
//                    // No explanation needed, we can request the permission.
////                    Logger.d(activity, "Request WRITE_EXTERNAL_STORAGE.");
//                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                            REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION);
//
//                    return true;
//                }
//            } else {
//                Log.e(TAG, "read permission already granted");
//                return false;
//            }
//        }
//        else
//        {
//            return false;
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//
//        Log.e(TAG, "onRequestPermissionsResult, requestCode: " + requestCode + ", result:" + grantResults[0]);
//
//        switch (requestCode) {
//            case REQ_PERMISSIONS_WRITE_EXTERNAL_STORAGE_PERMISSION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                    showPhotoDialog(false);
//                    Log.e(TAG, "Request WRITE_EXTERNAL_STORAGE is granted.");
//                    requestExternalStoragePermission();
//                } else {
//                    Log.e(TAG, "Request WRITE_EXTERNAL_STORAGE is denied.");
//                }
//            }
//            break;
//
//            case REQ_PERMISSIONS_READ_EXTERNAL_STORAGE_PERMISSION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                    showPhotoDialog(false);
//                    Log.e(TAG, "Request READ_EXTERNAL_STORAGE is granted.");
//                } else {
//                    Log.e(TAG, "Request READ_EXTERNAL_STORAGE is denied.");
//                }
//                return;
//            }
//
//        }
//    }

}
