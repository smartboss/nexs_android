package com.nexstreaming.china.mediastudio.internal.utils;

import android.content.res.Resources;

import java.util.Locale;

/**
 * Created by dimple on 22/12/2017.
 */

public class Utils {

    public static int dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return  (int)(dp * scale + 0.5f);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static String getTimeText(final int duration) {
        final int durationInSecond = duration / 1000;
        final int seconds = durationInSecond % 60;
        final int minutes = durationInSecond / 60;

        return String.format(Locale.ENGLISH,"%02d:%02d", minutes, seconds);
    }
}
