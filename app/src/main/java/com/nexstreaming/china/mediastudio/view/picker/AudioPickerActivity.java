package com.nexstreaming.china.mediastudio.view.picker;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.view.Template20Activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AudioPickerActivity extends AppCompatActivity implements View.OnClickListener, OnAudioFragmentInteractionListener {

    private final static String TAG = AudioPickerActivity.class.getSimpleName();

    final static int ACTION_CHOOSE_LOCAL_BGM = 10001;
    public final static String KEY_PATH = "key_path";

    public final static void goAudioPicker(Activity activity, final String currentPath) {
        Intent intent = new Intent(activity, AudioPickerActivity.class);
        if (!TextUtils.isEmpty(currentPath)) {
            intent.putExtra(KEY_PATH, currentPath);
        }
        activity.startActivityForResult(intent, Template20Activity.REQ_GET_BGM);
    }



    private ImageButton mImageButtonOk;

    private String mUsedLocalBgmPath = null;
    private String mSelectedLocalBgmPath = null;

    private MediaPlayer mMediaPlayer = null;

    private Map<String, String> mMapTemplateAudio = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_picker);

        mSelectedLocalBgmPath = mUsedLocalBgmPath = getIntent().getStringExtra(KEY_PATH);

        findViewById(R.id.back).setOnClickListener(this);
        mImageButtonOk = (ImageButton) findViewById(R.id.ok);
        mImageButtonOk.setOnClickListener(this);

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragments to adapter one by one
        adapter.addFragment(LocalAudioFragment.newInstance(null, null), getString(R.string.audio_picker_local_title));
        adapter.addFragment(MyAudioFragment.newInstance(null, null), getString(R.string.audio_picker_mine_title));
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        buildTemplateAudioRelationship();
    }

    @Override
    protected void onPause() {
        super.onPause();

        pauseAudio();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.ok:
                setResult(RESULT_OK, new Intent().putExtra(KEY_PATH, mSelectedLocalBgmPath));
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onFragmentInteraction(int action, Bundle data) {
        switch (action) {
            case ACTION_CHOOSE_LOCAL_BGM:
                if (data != null) {
                    mSelectedLocalBgmPath = data.getString(KEY_PATH);

                    pauseAudio();

                    if (!TextUtils.isEmpty(mSelectedLocalBgmPath)) {
                        play(mSelectedLocalBgmPath);
                    }

                    if (mUsedLocalBgmPath == null && mSelectedLocalBgmPath == null) {
                        mImageButtonOk.setVisibility(View.INVISIBLE);
                    } else if (mUsedLocalBgmPath != null && mSelectedLocalBgmPath != null && mUsedLocalBgmPath.equals(mSelectedLocalBgmPath)) {
                        mImageButtonOk.setVisibility(View.INVISIBLE);
                    } else {
                        mImageButtonOk.setVisibility(View.VISIBLE);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public String getCurrentBgmPath() {
        return mSelectedLocalBgmPath;
    }

    @Override
    public boolean isCurrentBgm(String path) {
        if (mSelectedLocalBgmPath == null) {
            return path == null;
        } else {
            return mSelectedLocalBgmPath.equals(path);
        }
    }

    private void buildTemplateAudioRelationship() {
        mMapTemplateAudio.clear();

//        // name / path
//        mMapTemplateAudio.put("3dbuilding2_live", "kmassets/3dbuilding2_live/audio/JSworks_swing_02/JSworks_swing_02.mp3");
//        mMapTemplateAudio.put("3dbuilding_live", "kmassets/3dbuilding_live/audio/JSworks_swing_01/JSworks_swing_01.mp3");
//        mMapTemplateAudio.put("3dflag_live", "kmassets/3dflag_live/audio/Tropical/Tropical.mp3");
//        mMapTemplateAudio.put("3dnews_live", "kmassets/3dnews_live/audio/JSworks_swing_02/JSworks_swing_02.mp3");
//        mMapTemplateAudio.put("3dpaper_live", "kmassets/3dpaper_live/audio/paper1/paper1.mp3");
//        mMapTemplateAudio.put("3drollpaper_live", "kmassets/3drollpaper_live/audio/dead/dead.mp3");
//        mMapTemplateAudio.put("90s Videos", "kmassets/90svideos/audio/RosyGirlMST2/RosyGirlMST2.mp3");
//        mMapTemplateAudio.put("EpicOpener", "kmassets/EpicOpener/audio/epicopener/epicopener.mp3");
//        mMapTemplateAudio.put("FunkyMove", "kmassets/FunkyMove/audio/FunkyMove/FunkyMove.m4a");
//        mMapTemplateAudio.put("LifeStyle", "kmassets/LifeStyle/audio/Future_Bass_Lifestyle/Future_Bass_Lifestyle.m4a");
//        mMapTemplateAudio.put("MidAutumn", "kmassets/MidAutumnFestival/audio/maf/maf.mp3");
//        mMapTemplateAudio.put("My Own Vlog", "kmassets/MyOwnVlog/audio/myownvlog_bgm/myownvlog_bgm.mp3");
//        mMapTemplateAudio.put("SpecialDay", "kmassets/SpecialDay/audio/Specialdays_re1/Specialdays_re1.mp3");
//        mMapTemplateAudio.put("TheWhiteView", "kmassets/TheWhiteView/audio/thewhiteview_bgm/thewhiteview_bgm.mp3");
//        mMapTemplateAudio.put("Travel", "kmassets/Travel/audio/MySD_bgm/MySD_bgm.mp3");
//        mMapTemplateAudio.put("breeze", "kmassets/breeze/audio/0110_Breeze MST/0110_Breeze MST.m4a");

        // bgmId / path
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dbuilding2_live.audio.JSworks_swing_02", "kmassets/3dbuilding2_live/audio/JSworks_swing_02/JSworks_swing_02.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dbuilding_live.audio.JSworks_swing_01", "kmassets/3dbuilding_live/audio/JSworks_swing_01/JSworks_swing_01.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dflag_live.audio.Tropical", "kmassets/3dflag_live/audio/Tropical/Tropical.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dnews_live.audio.JSworks_swing_02", "kmassets/3dnews_live/audio/JSworks_swing_02/JSworks_swing_02.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.3dpaper_live.audio.paper1", "kmassets/3dpaper_live/audio/paper1/paper1.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.3drollpaper_live.audio.dead", "kmassets/3drollpaper_live/audio/dead/dead.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.brix.nexeditor.template.90shomevideo.audio.RosyGirlMST2", "kmassets/90svideos/audio/RosyGirlMST2/RosyGirlMST2.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.epicopener.audio.epicopener", "kmassets/EpicOpener/audio/epicopener/epicopener.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.funkymove.audio.FunkyMove", "kmassets/FunkyMove/audio/FunkyMove/FunkyMove.m4a");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.maria.nexeditor.template.lifestyle.audio.Future_Bass_Lifestyle", "kmassets/LifeStyle/audio/Future_Bass_Lifestyle/Future_Bass_Lifestyle.m4a");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.midautumnfestival.audio.maf", "kmassets/MidAutumnFestival/audio/maf/maf.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.jiyeon.nexeditor.template.myownvlog.audio.myownvlog_bgm", "kmassets/MyOwnVlog/audio/myownvlog_bgm/myownvlog_bgm.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.design.nexeditor.template.cinematic_c.audio.Specialdays_re1", "kmassets/SpecialDay/audio/Specialdays_re1/Specialdays_re1.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.jiyeon.nexeditor.template.thewhiteview.audio.thewhiteview_bgm", "kmassets/TheWhiteView/audio/thewhiteview_bgm/thewhiteview_bgm.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.brix.nexeditor.template.travel.audio.MySD_bgm", "kmassets/Travel/audio/MySD_bgm/MySD_bgm.mp3");
        mMapTemplateAudio.put("com.nexstreaming.kmsdk.jieun.nexeditor.template.breeze.audio.0110_Breeze MST", "kmassets/breeze/audio/0110_Breeze MST/0110_Breeze MST.m4a");
    }

    private void play(final String path) {
        Log.e(TAG, "play: " + path);

        if (path.startsWith("com.nexstreaming.kmsdk")) {
            // assets
            final String assetPath = mMapTemplateAudio.get(path);
            if (TextUtils.isEmpty(assetPath)) {
                Log.e(TAG, "can't find asset audio path");
            } else {
                AssetFileDescriptor afd = null;
                try {
                    afd = getAssets().openFd(assetPath);
                    mMediaPlayer = new MediaPlayer();
                    mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mMediaPlayer.prepare();
                    mMediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            // external storage
//            mMediaPlayer = MediaPlayer.create(this, Uri.parse(Environment.getExternalStorageDirectory().getPath()+ "/Music/intro.mp3"));
            mMediaPlayer = MediaPlayer.create(this, Uri.parse(path));
            mMediaPlayer.setLooping(false);
            mMediaPlayer.start();
        }

    }

    private void pauseAudio() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            Fragment fragment = getItem(position);
            switch (position) {
                case 0:
                    ((LocalAudioFragment) fragment).refresh();
                    break;
                case 1:
                    ((MyAudioFragment) fragment).refresh();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
