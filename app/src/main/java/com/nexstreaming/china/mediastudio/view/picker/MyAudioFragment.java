package com.nexstreaming.china.mediastudio.view.picker;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.internal.loader.AudioManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnAudioFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyAudioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAudioFragment extends Fragment {
    private final static String TAG = MyAudioFragment.class.getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ArrayList<HashMap<String, String>> mList = new ArrayList<HashMap<String, String>>();

    private RecyclerView mRecyclerView;
    private MyAudioRecyclerViewAdapter mLocalAudioRecyclerViewAdapter;

    private OnAudioFragmentInteractionListener mListener;

    private AudioManager mAudioManager;

    public MyAudioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAudioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAudioFragment newInstance(String param1, String param2) {
        MyAudioFragment fragment = new MyAudioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mAudioManager = new AudioManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_audio, container, false);

        mRecyclerView = rootView.findViewById(R.id.recycler_view);

        mList.clear();
        mList = mAudioManager.getPlayList();
//        Log.e(TAG, "list size: " + mList.size());
//
//        for (HashMap hm : mList) {
//            Log.e(TAG, "hm: " + hm);
//        }

        rootView.findViewById(R.id.hint_no_audio).setVisibility(mList.size() == 0 ? View.VISIBLE : View.GONE);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mLocalAudioRecyclerViewAdapter = new MyAudioRecyclerViewAdapter(getContext());
        mRecyclerView.setAdapter(mLocalAudioRecyclerViewAdapter);

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(0, null);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAudioFragmentInteractionListener) {
            mListener = (OnAudioFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    void refresh() {
        mLocalAudioRecyclerViewAdapter.notifyDataSetChanged();
    }

    private class MyAudioRecyclerViewAdapter extends RecyclerView.Adapter<MyAudioRecyclerViewAdapter.ViewHolder> {

        private LayoutInflater inflater;

        // data is passed into the constructor
        MyAudioRecyclerViewAdapter(Context context) {
            this.inflater = LayoutInflater.from(context);
        }

        // inflates the cell layout from xml when needed
        @Override
        public MyAudioRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.recycler_item_local_audio, parent, false);
            return new MyAudioRecyclerViewAdapter.ViewHolder(view);
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(MyAudioRecyclerViewAdapter.ViewHolder holder, int position) {
            if (position == 0) {
                holder.textView.setText(R.string.audio_picker_no_audio);
            } else {
                holder.textView.setText(getItem(position).get(AudioManager.TITLE));
            }

            final boolean selected = mListener == null ? false : mListener.isCurrentBgm(position == 0 ? null : getItem(position).get(AudioManager.PATH));
            if (selected) {
                holder.textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
            } else {
                holder.textView.setTextColor(Color.WHITE);
            }
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return (mList == null ? 0 : mList.size() + 1);
        }

        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView textView;

            ViewHolder(View itemView) {
                super(itemView);

                textView = itemView.findViewById(R.id.title);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                onItemClicked(view, getAdapterPosition());
            }
        }

        // convenience method for getting data at click position
        private HashMap<String, String> getItem(int position) {
            if (position == 0) {
                return null;
            }
            return mList.get(position - 1);
        }

        private void onItemClicked(View view, int position) {
            if (mListener != null) {
                Bundle data = new Bundle();
                if (position == 0) {
                    data.putString(AudioPickerActivity.KEY_PATH, null);
                } else {
                    data.putString(AudioPickerActivity.KEY_PATH, getItem(position).get(AudioManager.PATH));
                }
                mListener.onFragmentInteraction(AudioPickerActivity.ACTION_CHOOSE_LOCAL_BGM, data);
            }

            mLocalAudioRecyclerViewAdapter.notifyDataSetChanged();
        }

    }
}
