package com.nexstreaming.china.mediastudio.view.widget;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.VideoView;

import com.nexstreaming.nexeditorsdk.nexEngine;

/**
 * Created by dimple on 09/02/2018.
 */

public class PlayController implements View.OnClickListener {
    private final static String TAG = PlayController.class.getSimpleName();

    public interface OnControlListener {
        public void onControlPause();
        public void onControlPlay();
    }

    enum STATUS {
        PLAYING,
        PAUSED
    }

    private View mViewScreen;
    private ImageView mImageViewStatus;
    private ImageButton mImageButton;

    private VideoView mVideoView;
    private nexEngine mEngine;

    private STATUS mStatus = STATUS.PAUSED;

    private OnControlListener mListener = null;

    public PlayController(nexEngine engine, VideoView videoView, View screen, ImageView status, ImageButton button) {
        mViewScreen = screen;
        mImageViewStatus = status;
        mImageButton = button;

        mViewScreen.setOnClickListener(this);

        if (mImageButton != null) {
            mImageButton.setOnClickListener(this);
        }

        mEngine = engine;
        mVideoView = videoView;
    }

    public void setListener(OnControlListener listener) {
        mListener = listener;
    }

    public void setPlaying() {
//        Log.e(TAG, "setPlaying");

        mImageViewStatus.setVisibility(View.INVISIBLE);

        if (mImageButton != null) {
            mImageButton.setSelected(false);
        }

        mStatus = STATUS.PLAYING;
    }

    public void setPaused() {
//        Log.e(TAG, "setPaused");

        mImageViewStatus.setVisibility(View.VISIBLE);

        if (mImageButton != null) {
            mImageButton.setSelected(true);
        }

        mStatus = STATUS.PAUSED;
    }

//    public void resume() {
//        mStatus = STATUS.PLAYING;
//    }

//    public void showView(final boolean set) {
//        final int v = set ? View.VISIBLE : View.INVISIBLE;
//        mViewScreen.setVisibility(v);
//
//        if (mImageButton != null) {
//            mImageButton.setVisibility(v);
//        }
//    }

    public void showScreen(final boolean set) {
        final int v = set ? View.VISIBLE : View.INVISIBLE;
        mViewScreen.setVisibility(v);
    }

    private void pauseInternal() {
        if (mVideoView == null) {
            mEngine.pause();
        } else {
            mVideoView.pause();
        }
        setPaused();

        if (mListener != null) {
            mListener.onControlPause();
        }
    }

    private void resumeInternal() {
        if (mVideoView == null) {
            mEngine.resume();
        } else {
            mVideoView.start();
        }
        setPlaying();

        if (mListener != null) {
            mListener.onControlPlay();
        }
    }

    public void close() {
        mViewScreen = null;
        mImageViewStatus = null;
        mImageButton = null;

        mEngine = null;
        mVideoView = null;
    }

    @Override
    public void onClick(View view) {
        switch (mStatus) {
            case PAUSED:
                resumeInternal();
                break;
            case PLAYING:
                pauseInternal();
                break;
            default:
                break;
        }
    }
}
