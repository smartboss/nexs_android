package com.nexstreaming.china.mediastudio.internal.loader;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;

import com.nexstreaming.china.mediastudio.MimeType;
import com.nexstreaming.china.mediastudio.internal.entity.Album;
import com.nexstreaming.china.mediastudio.internal.entity.Item;
import com.nexstreaming.china.mediastudio.internal.utils.MediaStoreCompat;

/**
 * Created by dimple on 22/12/2017.
 */

public class AlbumMediaLoader extends CursorLoader {
    private static final Uri QUERY_URI = MediaStore.Files.getContentUri("external");
    private static final String[] PROJECTION = {
            MediaStore.Files.FileColumns._ID,
            MediaStore.MediaColumns.DISPLAY_NAME,
            MediaStore.MediaColumns.MIME_TYPE,
            MediaStore.MediaColumns.SIZE,
            "duration"};

    // === params for album ALL && showSingleMediaType: false ===
    private static final String SELECTION_ALL =
            "(" + MediaStore.Files.FileColumns.MEDIA_TYPE + "=?"
                    + " OR "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE + "=?)"
                    + " AND " + MediaStore.MediaColumns.SIZE + ">0"
                    + " AND (" + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
//                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=?)"
                    + " AND (" + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.png' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.jpg' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.jpeg' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.webp' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.bmp' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.gif' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.mp4' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.mov') COLLATE NOCASE";
    private static final String[] SELECTION_ALL_ARGS = {
            String.valueOf(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE),
            String.valueOf(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO),
            MimeType.PNG.toString(), MimeType.JPEG.toString(), MimeType.WEBP.toString(), MimeType.BMP.toString(), MimeType.GIF.toString(),
            MimeType.MP4.toString(),/* MimeType.THREEGPP.toString(),*/ MimeType.QUICKTIME.toString(),
    };

    private static final String SELECTION_VIDEO_ONLY =
            "(" + MediaStore.Files.FileColumns.MEDIA_TYPE + "=?)"
                    + " AND " + MediaStore.MediaColumns.SIZE + ">0"
                    + " AND (" + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
//                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=?)"
                    + " AND (" + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.mp4' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.mov') COLLATE NOCASE";
    private static final String[] SELECTION_VIDEO_ONLY_ARGS = {
            String.valueOf(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO),
            MimeType.MP4.toString(),/* MimeType.THREEGPP.toString(),*/ MimeType.QUICKTIME.toString(),
    };

    private static final String SELECTION_IMAGE_ONLY =
            "(" + MediaStore.Files.FileColumns.MEDIA_TYPE + "=?)"
                    + " AND " + MediaStore.MediaColumns.SIZE + ">0"
                    + " AND (" + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR "
                    + MediaStore.Files.FileColumns.MIME_TYPE + "=?)"
                    + " AND (" + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.png' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.jpg' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.jpeg' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.webp' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.bmp' OR "
                    + MediaStore.MediaColumns.DISPLAY_NAME + " LIKE '%.gif') COLLATE NOCASE";
    private static final String[] SELECTION_IMAGE_ONLY_ARGS = {
            String.valueOf(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE),
            MimeType.PNG.toString(), MimeType.JPEG.toString(), MimeType.WEBP.toString(), MimeType.BMP.toString(), MimeType.GIF.toString(),
    };

    private static final String[] SELECTION_VIDEO = {
            String.valueOf(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO),
    };
    // ===========================================================

    // === params for album ALL && showSingleMediaType: true ===
    private static final String SELECTION_ALL_FOR_SINGLE_MEDIA_TYPE =
            MediaStore.Files.FileColumns.MEDIA_TYPE + "=?"
                    + " AND " + MediaStore.MediaColumns.SIZE + ">0";

    private static String[] getSelectionArgsForSingleMediaType(int mediaType) {
        return new String[]{String.valueOf(mediaType)};
    }
    // =========================================================

    // === params for ordinary album && showSingleMediaType: false ===
    private static final String SELECTION_ALBUM =
            "(" + MediaStore.Files.FileColumns.MEDIA_TYPE + "=?"
                    + " OR "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE + "=?)"
                    + " AND "
                    + " bucket_id=?"
                    + " AND " + MediaStore.MediaColumns.SIZE + ">0";

    private static String[] getSelectionAlbumArgs(String albumId) {
        return new String[]{
                String.valueOf(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE),
                String.valueOf(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO),
                albumId
        };
    }
    // ===============================================================

    // === params for ordinary album && showSingleMediaType: true ===
    private static final String SELECTION_ALBUM_FOR_SINGLE_MEDIA_TYPE =
            MediaStore.Files.FileColumns.MEDIA_TYPE + "=?"
                    + " AND "
                    + " bucket_id=?"
                    + " AND " + MediaStore.MediaColumns.SIZE + ">0";

    private static String[] getSelectionAlbumArgsForSingleMediaType(int mediaType, String albumId) {
        return new String[]{String.valueOf(mediaType), albumId};
    }
    // ===============================================================

    private static final String ORDER_BY = MediaStore.Images.Media.DATE_TAKEN + " DESC";
    private final boolean mEnableCapture;

    private AlbumMediaLoader(Context context, String selection, String[] selectionArgs, boolean capture) {
        super(context, QUERY_URI, PROJECTION, selection, selectionArgs, ORDER_BY);
        mEnableCapture = capture;
    }

    public static CursorLoader newInstance(Context context, Album album, boolean capture, boolean imageOnly, boolean isVideoOnly) {
        String selection;
        String[] selectionArgs;
        boolean enableCapture;

        if (album.isAll()) {
//            if (SelectionSpec.getInstance().onlyShowImages()) {
//                selection = SELECTION_ALL_FOR_SINGLE_MEDIA_TYPE;
//                selectionArgs = getSelectionArgsForSingleMediaType(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);
//            } else if (SelectionSpec.getInstance().onlyShowVideos()) {
//                selection = SELECTION_ALL_FOR_SINGLE_MEDIA_TYPE;
//                selectionArgs = getSelectionArgsForSingleMediaType(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO);
//            } else {
//                selection = SELECTION_ALL;
//                selectionArgs = SELECTION_ALL_ARGS;
//            }

            if (imageOnly) {
//                selection = SELECTION_ALL_FOR_SINGLE_MEDIA_TYPE;
//                selectionArgs = getSelectionArgsForSingleMediaType(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);

                selection = SELECTION_IMAGE_ONLY;
                selectionArgs = SELECTION_IMAGE_ONLY_ARGS;
            } else if(isVideoOnly){
//                selection = SELECTION_ALL_FOR_SINGLE_MEDIA_TYPE;
//                selectionArgs = getSelectionArgsForSingleMediaType(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO);

                selection = SELECTION_VIDEO_ONLY;
                selectionArgs = SELECTION_VIDEO_ONLY_ARGS;
            }
            else {
                selection = SELECTION_ALL;
                selectionArgs = SELECTION_ALL_ARGS;
            }

            enableCapture = capture;
        } else {
//            if (SelectionSpec.getInstance().onlyShowImages()) {
//                selection = SELECTION_ALBUM_FOR_SINGLE_MEDIA_TYPE;
//                selectionArgs = getSelectionAlbumArgsForSingleMediaType(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE,
//                        album.getId());
//            } else if (SelectionSpec.getInstance().onlyShowVideos()) {
//                selection = SELECTION_ALBUM_FOR_SINGLE_MEDIA_TYPE;
//                selectionArgs = getSelectionAlbumArgsForSingleMediaType(MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO,
//                        album.getId());
//            } else {
//                selection = SELECTION_ALBUM;
//                selectionArgs = getSelectionAlbumArgs(album.getId());
//            }

            selection = SELECTION_ALBUM;
            selectionArgs = getSelectionAlbumArgs(album.getId());

            enableCapture = false;
        }
        return new AlbumMediaLoader(context, selection, selectionArgs, enableCapture);
    }

    @Override
    public Cursor loadInBackground() {
        Cursor result = super.loadInBackground();
        if (!mEnableCapture || !MediaStoreCompat.hasCameraFeature(getContext())) {
            return result;
        }
        MatrixCursor dummy = new MatrixCursor(PROJECTION);
        dummy.addRow(new Object[]{Item.ITEM_ID_CAPTURE, Item.ITEM_DISPLAY_NAME_CAPTURE, "", 0, 0});
        return new MergeCursor(new Cursor[]{dummy, result});
    }

    @Override
    public void onContentChanged() {
        // FIXME a dirty way to fix loading multiple times
    }
}
