package com.nexstreaming.china.mediastudio.internal.loader;

/**
 * Created by dimple on 29/01/2018.
 */

import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

public class AudioManager {
    private final static String TAG = AudioManager.class.getSimpleName();

    public final static String TITLE = "songTitle";
    public final static String PATH = "songPath";

    // SDCard Path
    //choose your path for me i choose sdcard
    final String MEDIA_PATH = new String("/sdcard/");
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<>();

    // Constructor
    public AudioManager(){

    }

    /**
     * Function to read all mp3 files from sdcard
     * and store the details in ArrayList
     * */
    public ArrayList<HashMap<String, String>> getPlayList() {

        walk(new File(MEDIA_PATH));
        // return songs list array
        return songsList;
    }

    private void walk(File root) {

        File[] list = root.listFiles();
        for (File f : list) {
            if (f.isDirectory()) {
//                Log.e(TAG, "Dir: " + f.getAbsoluteFile());

                if (root.listFiles(new FileExtensionFilter()).length > 0) {
                    for (File file : root.listFiles(new FileExtensionFilter())) {
                        HashMap song = new HashMap();
                        song.put(TITLE, file.getName().substring(0, (file.getName().length() - 4)));
                        song.put(PATH, file.getPath());

                        // Adding each song to SongList
                        songsList.add(song);
                    }
                }

                walk(f);
            } else {
//                Log.d(TAG, "File: " + f.getAbsoluteFile());
            }
        }

    }

    /**
     * Class to filter files which are having .mp3 extension
     * */
    //you can choose the filter for me i put .mp3
    class FileExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".mp3") || name.endsWith(".MP3"));
        }
    }
}