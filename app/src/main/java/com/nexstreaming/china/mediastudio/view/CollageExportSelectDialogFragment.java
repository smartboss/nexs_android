package com.nexstreaming.china.mediastudio.view;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;

import com.nexstreaming.china.mediastudio.R;

/**
 * Created by marklin on 2018/1/14.
 */

public class CollageExportSelectDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = CollageExportSelectDialogFragment.class.getSimpleName();

    private static CollageExportSelectDialogFragment collageExportSelectDialogFragment;

    private OnExportDialogCallback onExportDialogCallback;
    private ImageButton btnClose, btnVideo, btnImage;

    public static CollageExportSelectDialogFragment newInstance() {
        if (collageExportSelectDialogFragment == null) {
            collageExportSelectDialogFragment = new CollageExportSelectDialogFragment();
        }

        return collageExportSelectDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_collage_exprot, container, false);

        btnClose = (ImageButton) view.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);

        btnImage = (ImageButton) view.findViewById(R.id.btn_export_image);
        btnImage.setOnClickListener(this);

        btnVideo = (ImageButton) view.findViewById(R.id.btn_export_video);
        btnVideo.setOnClickListener(this);

        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    public void showDialog(FragmentManager fragmentManager, OnExportDialogCallback onExportDialogCallback) {
        show(fragmentManager, null);
        this.onExportDialogCallback = onExportDialogCallback;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                break;
            case R.id.btn_export_image:
                if (onExportDialogCallback != null) {
                    onExportDialogCallback.OnImageSelect();
                }
                break;
            case R.id.btn_export_video:
                if (onExportDialogCallback != null) {
                    onExportDialogCallback.OnVideoSelect();
                }
                break;
        }

        dismiss();
    }

    public interface OnExportDialogCallback {
        void OnImageSelect();

        void OnVideoSelect();
    }
}
