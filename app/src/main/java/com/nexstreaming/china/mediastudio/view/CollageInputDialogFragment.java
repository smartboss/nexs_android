package com.nexstreaming.china.mediastudio.view;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.nexeditorsdk.nexCollageInfoTitle;

import java.util.Locale;

/**
 * Created by marklin on 2018/1/14.
 */

public class CollageInputDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = CollageInputDialogFragment.class.getSimpleName();

    private static CollageInputDialogFragment collageInputDialogFragment;
    private OnDialogCallback onDialogCallback;

    private EditText etMessage;
    private ImageButton btnCancel, btnConfirm;
    private nexCollageInfoTitle nexCollageInfoTitle;

    public static CollageInputDialogFragment newInstance(Bundle bundle) {
        if (collageInputDialogFragment == null) {
            collageInputDialogFragment = new CollageInputDialogFragment();
        }

        collageInputDialogFragment.setArguments(bundle);
        return collageInputDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_collage_input, container, false);
        setCancelable(false);

        etMessage = (EditText) view.findViewById(R.id.et_message);

        btnCancel = (ImageButton) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

        btnConfirm = (ImageButton) view.findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                if (onDialogCallback != null) {
                    onDialogCallback.Cancel();
                }
                break;
            case R.id.btn_confirm:
                if (onDialogCallback != null) {
                    if (TextUtils.isEmpty(etMessage.getText().toString())) {
                        onDialogCallback.Cancel();
                    } else {
                        onDialogCallback.Confirm(etMessage.getText().toString());
                    }
                }
                break;
        }

        dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();

        InputFilter[] filterArray = new InputFilter[2];
        filterArray[0] = new InputFilter.LengthFilter(nexCollageInfoTitle.getTitleMaxLength());
        filterArray[1] = getEditTextFilterEmoji();
        etMessage.setFilters(filterArray);

        etMessage.setMaxLines(nexCollageInfoTitle.getTitleMaxLines());
        etMessage.setText(nexCollageInfoTitle.getTitle(Locale.getDefault().getLanguage()));
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
    }

    public void showDialog(FragmentManager fragmentManager, nexCollageInfoTitle nexCollageInfoTitle, OnDialogCallback onDialogCallback) {
        show(fragmentManager, null);
        this.onDialogCallback = onDialogCallback;
        this.nexCollageInfoTitle = nexCollageInfoTitle;
    }

    public static InputFilter getEditTextFilterEmoji() {
        return new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                for (int i = start; i < end; i++) {
                    int type = Character.getType(source.charAt(i));
                    if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                        return source.subSequence(start, i);
                    }
                }
                return null;
            }
        };
    }

    public interface OnDialogCallback {
        void Confirm(String text);

        void Cancel();
    }
}
