/******************************************************************************
 * File Name        : Constants.java
 * Description      :
 *******************************************************************************
 * Copyright (c) 2002-2017 NexStreaming Corp. All rights reserved.
 * http://www.nexstreaming.com
 *
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
 * PURPOSE.
 ******************************************************************************/

package com.nexstreaming.nexeditorsdkapis.common;

/**
 * Created by rooney.park on 2015-12-08.
 */
public class Constants {
    /* Effect */
    public static final String EFFECT = "Effect";
    public static final String TRANSITION_EFFECT = "Effect";

    public static final String EFFECT_OPTION = "EffectOption";
    public static final String TRANSITION_EFFECT_OPTION = "TransitionEffectOption";
    public static final String OVERLAY_FILTER_OPTION = "OverlayFilterOption";

    public static final String THEME_ID = "ThemeID";
    public static final String TRANSITION_ID = "TransitionID";
    public static final String OVERLAY_FILTER_ID = "OverlayFilterID";

    public static final String EFFECT_TYPE = "EffectType";

    public static final int EFFECT_TYPE_THEME = 0;
    public static final int EFFECT_TYPE_RANDOM_TRANSITION = 1;
    public static final int EFFECT_TYPE_TRANSITION = 2;
    public static final int EFFECT_TYPE_CLIP = 3;
    public static final int EFFECT_TYPE_SELECT_TRANSITION = 4;
    public static final int EFFECT_TYPE_SELECT_CLIP = 5;
    public static final int EFFECT_TYPE_COLOR = 6;
    public static final int EFFECT_TYPE_STICKER = 7;
    public static final int EFFECT_TYPE_FONT = 8;
    public static final int EFFECT_TYPE_SELECT_OVERLAYFILTER = 9;

    public static final String TITLE = "TITLE";
    public static final String SUB_TITLE = "SubTitle";
}
