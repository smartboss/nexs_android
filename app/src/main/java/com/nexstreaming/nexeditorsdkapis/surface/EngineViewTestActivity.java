package com.nexstreaming.nexeditorsdkapis.surface;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.nexstreaming.china.mediastudio.R;
import com.nexstreaming.china.mediastudio.view.VideoTrimActivity;
import com.nexstreaming.nexeditorsdk.nexClip;
import com.nexstreaming.nexeditorsdk.nexCrop;
import com.nexstreaming.nexeditorsdk.nexEngine;
import com.nexstreaming.nexeditorsdk.nexEngineView;
import com.nexstreaming.nexeditorsdk.nexProject;
import com.nexstreaming.nexeditorsdkapis.ApiDemosConfig;
import com.nexstreaming.nexeditorsdkapis.common.RangeSeekBar;
import com.nexstreaming.nexeditorsdkapis.common.VideoTimeLineView;
import com.nexstreaming.scmbsdk.nxSCMBEntry;
import com.nexstreaming.scmbsdk.nxSCMBSDK;

import java.util.ArrayList;

public class EngineViewTestActivity extends Activity {
    private final static String TAG = "EngineViewActivity";
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent();
            intent.putExtra("filelist", getIntent().getStringArrayListExtra("filelist"));
            intent.setClass(EngineViewTestActivity.this, VideoTrimActivity.class);
            startActivity(intent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engine_view_test);

        mHandler.postDelayed(mRunnable, 1000);
    }
}
